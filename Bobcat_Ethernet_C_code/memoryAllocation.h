#define ALIGN 32
#define UNALLOCATED (void *)1
template<typename T>
void free1D(T*& a)
{
	if (a)
	{
		_aligned_free(a);
		a = 0;
	}
	a = 0;
}
template<typename T>
void free2D(T**& a)
{
	if (a)
	{
		//			T **row;
		_aligned_free(&a[0][0]);
		_aligned_free(a);
	}
	a = 0;
}

template<typename T>
void free2D_Jagged(T**& a)
{

	if (a)
	{
		T** row;
		//_aligned_free(&a[0][0]);
		for (row = a; *row != 0; row++)
		{
			if (*row != UNALLOCATED)
			{
				_aligned_free(*row);
			}
		}
		_aligned_free(a);
	}
	a = 0;
}
template<typename T>
T allocateJagged(int n, T a)
{
	if (a)
	{
		//free1D<T>(a);
		_aligned_free(a);
	}
	T ptr = (T)_aligned_malloc(sizeof(T) * n, ALIGN);
	for (int i = 0; i < n; i++)
	{
		ptr[i] = 0;
	}
	return ptr;
}

template<typename T>
bool allocate1D(size_t n, T*& a)
{
	if (a)
	{
		free1D<T>(a);
	}
	if (n > 0)
	{
		a = (T*)_aligned_malloc(sizeof(T) * n, ALIGN);
	}
	else
	{
		a = 0;
	}
	return (a != 0);
}

template<typename T>
void allocate2D(size_t nrows, size_t ncols, T**& a)
{
	// T **a;
	if (a)
	{
		free2D<T>(a);
	}
	// size_t i;
	size_t i;
	a = (T**)_aligned_malloc(sizeof(T*) * (nrows + 1), ALIGN);
	T* a1D = (T*)_aligned_malloc(sizeof(T) * nrows * ncols, ALIGN);
	if (a != 0) //return 0;
	{
		/* now allocate the actual rows */
		for (i = 0; i < nrows; i++)
		{
			//a[i] = (T*)_aligned_malloc(sizeof(T)*ncols, ALIGN);
			a[i] = &a1D[i * ncols];
			//						 if(a[i] == 0) {return 0;}
		}
		a[nrows] = 0;
	}
	// return a;
}

template<typename T>
void allocate2D_Jagged(int nrows, T**& a)
{
	// T **a;
	if (a)
	{//These are not being freed correctly
		free2D_Jagged<T>(a);
	}
	// size_t i;
	size_t i;
	a = (T**)_aligned_malloc(sizeof(T*) * (nrows + 1), ALIGN);

	if (a != 0) //return 0;
	{
		/* now allocate the actual rows */
		for (i = 0; i < nrows; i++)
		{
			a[i] = (T*)UNALLOCATED;// 0;// (T*)_aligned_malloc(sizeof(T)*ncols, ALIGN);
			//a[i] = &a1D[i*ncols];
			//						 if(a[i] == 0) {return 0;}
		}
		a[nrows] = 0;
	}
	// return a;
}

template<typename T>
void allocate3D(size_t nx, size_t ny, size_t nz, T***& a)
{

	if (a)
	{
		free3D(a);
	}

	int i;
	int j;
	a = (T***)_aligned_malloc(sizeof(T**) * (nx + 1), ALIGN); /* one extra for sentinel */
	T* a1D = (T*)_aligned_malloc(sizeof(T) * (nx * ny * nz), ALIGN);
	for (i = 0; i < nx; i++)
	{
		a[i] = (T**)_aligned_malloc(sizeof(T*) * (ny + 1), ALIGN);
		for (j = 0; j < ny; j++)
		{
			//a[i][j] = (T*)_aligned_malloc(sizeof(T)*nz, ALIGN);
			a[i][j] = &a1D[i * ny * nz + j * nz];
		}
		a[i][ny] = 0;
	}
	a[nx] = 0;
}

template<typename T>
void allocate3D_Jagged(size_t nx, size_t ny, T***& a)
{
	if (a)
	{
		free3D_Jagged(a);
	}
	size_t i;
	size_t j;
	a = (T***)_aligned_malloc(sizeof(T**) * (nx + 1), ALIGN); /* one extra for sentinel */
	//if(a == 0) return 0;
	/* now allocate the actual rows */
	for (i = 0; i < nx; i++)
	{
		a[i] = (T**)_aligned_malloc(sizeof(T*) * (ny + 1), ALIGN);
		// if(a[i] == 0) {return 0;}
		for (j = 0; j < ny; j++)
		{
			a[i][j] = UNALLOCATED;// 0;// (T*)_aligned_malloc(sizeof(T)*nz, ALIGN);
			// if (a[i][j] == 0) {return 0;}
		}
		a[i][ny] = 0;
	}
	a[nx] = 0;
	// return a;
}

template<typename T>
T*** allocate3D_Jagged(size_t nx, size_t ny)
{
	size_t i;
	size_t j;
	T*** a = (T***)_aligned_malloc(sizeof(T**) * (nx + 1), ALIGN); /* one extra for sentinel */
	//if(a == 0) return 0;
	/* now allocate the actual rows */
	for (i = 0; i < nx; i++)
	{
		a[i] = (T**)_aligned_malloc(sizeof(T*) * (ny + 1), ALIGN);
		// if(a[i] == 0) {return 0;}
		for (j = 0; j < ny; j++)
		{
			a[i][j] = UNALLOCATED;// 0;// (T*)_aligned_malloc(sizeof(T)*nz, ALIGN);
			// if (a[i][j] == 0) {return 0;}
		}
		a[i][ny] = 0;
	}
	a[nx] = 0;
	return a;
}

template<typename T>
void allocate4D(size_t nx, size_t ny, size_t nz, size_t nw, T****& a)
{
	//int nw = 0;
	if (a)
	{
		free4D(a);
	}

	size_t i;
	size_t j;
	a = (T****)_aligned_malloc(sizeof(T***) * (nx + 1), ALIGN); // one extra for sentinel
	//	T *a1D = (T*)_aligned_malloc(sizeof(T)*nx*ny*nz*nw, ALIGN);
	//if(a == 0) return 0;
	T* a1D = (T*)_aligned_malloc(sizeof(T) * nx * ny * nz * nw, ALIGN);

	// now allocate the actual rows
	for (i = 0; i < nx; i++)
	{
		a[i] = (T***)_aligned_malloc(sizeof(T**) * (ny + 1), ALIGN);
		// if(a[i] == 0) {return 0;}
		for (j = 0; j < ny; j++)
		{
			a[i][j] = (T**)_aligned_malloc(sizeof(T*) * (nz + 1), ALIGN);
			for (int k = 0; k < nz; k++)
			{
				//a[i][j][k] = (T*)_aligned_malloc(sizeof(T)*(nw + 1), ALIGN);
				//a[i][j][k] = &a1D[i*ny*nz*nw + j*nz*nw + k*nw];
				a[i][j][k] = &a1D[i * ny * nz * nw + j * nz * nw + k * nw];
			}
			a[i][j][nz] = 0;
		}
		a[i][ny] = 0;
	}
	a[nx] = 0;
}
template<typename T>
void allocate4D_Jagged(size_t nx, size_t  ny, size_t  nz, T****& a)
{

	if (a)
	{
		free4D_Jagged(a);
	}
	size_t i;
	size_t j;
	a = (T****)_aligned_malloc(sizeof(T***) * (nx + 1), ALIGN); /* one extra for sentinel */
	//if(a == 0) return 0;
	/* now allocate the actual rows */
	for (i = 0; i < nx; i++)
	{
		a[i] = (T***)_aligned_malloc(sizeof(T**) * (ny + 1), ALIGN);
		// if(a[i] == 0) {return 0;}
		for (j = 0; j < ny; j++)
		{
			a[i][j] = (T**)_aligned_malloc(sizeof(T*) * (nz + 1), ALIGN);
			for (int k = 0; k < nz; k++)
			{
				//a[i][j][k] = 0;// (T*)_aligned_malloc(sizeof(T)*(nw + 1));
				a[i][j][k] = (T*)UNALLOCATED;// (T*)_aligned_malloc(sizeof(T)*(1));
			}
			a[i][j][nz] = 0;
			// if (a[i][j] == 0) {return 0;}
		}
		a[i][ny] = 0;
	}
	a[nx] = 0;
	// return a;
}


template<typename T>
void free3D(T***& arr3D)
{
	if (arr3D)
	{
		int i = 0;
		int j = 0;
		_aligned_free(&arr3D[0][0][0]);
		while (arr3D[i] != 0)
		{
			_aligned_free(arr3D[i]);
			i++;
		}

		_aligned_free(arr3D);
	}
	arr3D = 0;
}

template<typename T>
void free3D_Jagged(T***& arr3D)
{
	//int i,j; 

	if (arr3D)
	{
		int i = 0;
		int j = 0;
		//for(i=0;i<l;i++) 
		while (arr3D[i] != 0)
		{
			j = 0;
			//for(int j=0;j<m;j++) 
			while (arr3D[i][j] != 0)
			{
				if (arr3D[i][j] != UNALLOCATED)
				{
					_aligned_free(arr3D[i][j]);
				}
				j++;
			}
			_aligned_free(arr3D[i]);
			i++;
		}

		_aligned_free(arr3D);
	}
	arr3D = 0;
}

template<typename T>
void free4D(T****& arr4D)
{
	//int i,j; 

	if (arr4D)
	{
		int i = 0;
		int j = 0;
		int k = 0;
		_aligned_free(&arr4D[0][0][0][0]);
		//for(i=0;i<l;i++) 
		while (arr4D[i] != 0)
		{
			j = 0;
			//for(int j=0;j<m;j++) 
			while (arr4D[i][j] != 0)
			{
				//k = 0;
				//while (arr4D[i][j][k] != 0)
				//{
				//	_aligned_free(arr4D[i][j][k]);
				//	k++;
				//}
				_aligned_free(arr4D[i][j]);
				j++;
			}
			_aligned_free(arr4D[i]);
			i++;
		}

		_aligned_free(arr4D);
	}

	arr4D = 0;
	//System::Diagnostics::Debug::WriteLine("Finished 3D free");
}
template<typename T>
void free4D_Jagged(T****& arr4D)
{
	//int i,j; 

	if (arr4D)
	{
		int i = 0;
		int j = 0;
		int k = 0;
		//for(i=0;i<l;i++) 
		while (arr4D[i] != 0)
		{
			j = 0;
			//for(int j=0;j<m;j++) 
			while (arr4D[i][j] != 0)
			{
				k = 0;
				while (arr4D[i][j][k] != 0)
				{
					if (arr4D[i][j][k] != UNALLOCATED)
					{
						_aligned_free(arr4D[i][j][k]);
					}
					k++;
				}
				j++;
			}
			_aligned_free(arr4D[i]);
			i++;
		}

		_aligned_free(arr4D);
	}
	arr4D = 0;
	//System::Diagnostics::Debug::WriteLine("Finished 3D free");
}

template<typename T>
T*** resizeArray3D(size_t oldX, size_t oldY, size_t oldZ, size_t newX, size_t newY, size_t newZ, T*** oldArray3D)
{
	T*** newArray3D = 0;
	allocate3D(newX, newY, newZ, newArray3D);
	for (size_t i = 0; i < newX; i++)
	{
		T** newI = newArray3D[i];
		if (i < oldX)
		{
			T** oldI = oldArray3D[i];
			for (size_t j = 0; j < newY; j++)
			{
				T* newIJ = newI[j];
				if (j < oldY)
				{
					T* oldIJ = oldI[j];
					for (size_t k = 0; k < newZ; k++)
					{
						if (k < oldZ)
						{
							newIJ[k] = oldIJ[k];
						}
						else
						{
							newIJ[k] = 0;
						}
					}
				}
				else
				{
					memset(&newArray3D[i][j][0], 0, sizeof(T) * newZ);
				}
			}
		}
		else
		{
			memset(&newArray3D[i][0][0], 0, sizeof(T) * newY * newZ);
		}
	}
	free3D(oldArray3D);
	return newArray3D;
}

template<typename T>
T** resizeArray2D(size_t oldX, size_t oldY, size_t newX, size_t newY, T** oldArray2D)
{
	T** newArray2D = 0;
	allocate2D(newX, newY, newArray2D);
	for (size_t i = 0; i < newX; i++)
	{
		T* newI = newArray2D[i];
		if (i < oldX)
		{
			T* oldI = oldArray2D[i];
			for (size_t j = 0; j < newY; j++)
			{
				if (j < oldY)
				{
					newI[j] = oldI[j];
				}
				else
				{
					newI[j] = 0;
				}
			}//j
		}
		else
		{//row doesn't exist in the old array, so just write zeros to it
			memset(&newArray2D[i][0], 0, sizeof(T) * newY);
		}
	}//i
	free2D(oldArray2D);
	return newArray2D;
}//function


//Delegated controls
