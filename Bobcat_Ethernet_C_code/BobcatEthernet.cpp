#include "BobcatEthernet.h"
#include "memoryAllocation.h"


//#define TRANSPOSEIT //transpose the dimensions width and height
//Translates the capture mode enumerations between different Xenics cameras
#define CAPTUREMODE_CONT 1
#define CAPTUREMODE_SINGLESHOT 0
#define CAPTUREMODE_TRIGGER 3
#define CAPTUREMODE_SEQUENCE 2

int captureModeTranslation[4];

T0 enumerate_all_connected_xenics_cameras() {

	T0 output;

	if ((output.errorCode = XCD_EnumerateDevices(NULL, &output.device_count, XEF_EnableAll)) != I_OK) {
		XC_ErrorToString(output.errorCode, output.error_str, error_str_size);
	}
	return output;
}

T1 print_connected_cameras_info(unsigned int deviceCount) {
	T1 output;
	
	XDeviceInformation* all_devices = new XDeviceInformation[deviceCount];
	if ((output.errorCode = XCD_EnumerateDevices(all_devices, &deviceCount, XEF_UseCached)) != I_OK) {
		XC_ErrorToString(output.errorCode, output.error_str, error_str_size);
		delete[] all_devices;
	}
	
	for (unsigned int i = 0; i < deviceCount; i++) {
		output.devices[i] = all_devices[i]; // The warning is not a problem. The 'all_devices' is initialized internally in XCD_EnumerateDevices
	}

	return output;
}

XCHANDLE init_camera(char* URL){
	
	XCHANDLE handle = XC_OpenCamera(URL);
	if (XC_IsInitialised(handle))
	{	
		//Set the timeout to 100ms. To keep the camera responsive. We don't want to wait that long for a new frame if there isn't one.
		long timeout = 0;
		XC_SetPropertyValueL(handle, "_API_GETFRAME_TIMEOUT", 100, "");
		XC_GetPropertyValueL(handle, "_API_GETFRAME_TIMEOUT", &timeout);
		printf("Timeout (ms) = %i\n", timeout);			

		return handle;
	}
	else
	{	
		return -1;
	}
}

int close_camera(XCHANDLE handle) {

	if (XC_IsInitialised(handle))
	{
		XC_CloseCamera(handle);
		return 0;
	}
	else 
	{
		return -1;
	}
}


T2 start_capture(XCHANDLE handle) {
	T2 output;

	if (XC_IsInitialised(handle)) {

		if ((output.errorCode = XC_StartCapture(handle)) != I_OK)
		{
			XC_ErrorToString(output.errorCode, output.error_str, error_str_size);
			output.return_value = -2; // Python: -2 is for camera initialized but some other problem
		}
		else
		{
			output.return_value = 0; // Python: 0 means all good!
		}
	}
	else 
	{
		output.return_value = -1; //Python: -1 is for camera not even initialized
	}
	return output;
}


T2 stop_capture(XCHANDLE handle) {
	T2 output;
	
	if (XC_IsCapturing(handle))
	{
		if ((output.errorCode = XC_StopCapture(handle)) != I_OK)
		{
			XC_ErrorToString(output.errorCode, output.error_str, error_str_size);
			output.return_value = -2; // Python: -2 is for camera initialized but some other problem
		}
		else 
		{
			output.return_value = 0; // Python: 0 means all good!
		}
	}
	else
	{
		output.return_value = -1; //Python: -1 is for camera not even initialized
	}
	return output;
}


T2 get_frame(XCHANDLE handle, unsigned short* frame) {
	T2 output;
	double fps = 0;
	__int64 startTime = QPC();
	if ((output.errorCode = XC_GetFrame(handle, FT_NATIVE, XGF_Blocking, frame, XC_GetFrameSize(handle))) != I_OK) //XGF_NoConversion  XGF_Blocking
	{
		XC_ErrorToString(output.errorCode, output.error_str, error_str_size);
		output.return_value = -1; // Python: -1 some problem getting the frame
		
	}
	else 
	{
		output.return_value = 0; // Python: 0 all good
		/*__int64 stopTime = QPC();
		double eTime = QPCTimeElapsedSeconds(startTime, stopTime);
		fps = (1) / eTime;
		printf("fps = %f\n", fps);*/
	}
	return output;
}

T3 get_exposure_time(XCHANDLE handle) {
	T3 output;

	output.errorCode = XC_GetPropertyValueF(handle, "ExposureTime", &output.exposure_time);
	XC_ErrorToString(output.errorCode, output.error_str, error_str_size);

	return output;
}

XErr set_exposure_time(XCHANDLE handle, double exposure_time_in_us) {
	XErr output;

	output.errorCode = XC_SetPropertyValueF(handle, "ExposureTime", exposure_time_in_us, "");
	XC_ErrorToString(output.errorCode, output.error_str, error_str_size);

	return output;
}

T4 turn_on_software_correct(XCHANDLE handle, const char* packpath) {
	T4 output;

	if (I_OK == XC_LoadCalibration(handle, packpath, 0))
	{
		output.software_correction_filterID = XC_FLT_Queue(handle, "SoftwareCorrection", 0);

		output.filter_running = XC_IsFilterRunning(handle, output.software_correction_filterID); //Checks whether the filter is really applied
		
		// Set KeepOffset to 1 and checks if set
		int len = 1;
		char value = '\0';
		XC_FLT_SetParameter(handle, output.software_correction_filterID, "KeepOffset", "1");
		XC_FLT_GetParameter(handle, output.software_correction_filterID, "KeepOffset", (char*)&value, &len);
		//printf("KeepOffset now set to: %c\n", value);

		output.return_value = 0;
	}
	else 
	{
		output.return_value = -1;
	}

	return output;
}


void get_camera_param(XCHANDLE handle, int& height, int&width, int&bitsize)
{

	// Get width and height of the camera
#ifdef TRANSPOSEIT
	height = XC_GetWidth(handle);
	width = XC_GetHeight(handle);
#else
	width = XC_GetWidth(handle);
	height = XC_GetHeight(handle);
#endif

	// get the bit size
	bitsize = XC_GetBitSize(handle);
	printf("width = %d\t height = %d\n", width, height);

}

void define_captureMode() {

captureModeTranslation[CAPTUREMODE_SINGLESHOT] = 1;//Triggered
captureModeTranslation[CAPTUREMODE_CONT] = 0;//Free run
captureModeTranslation[CAPTUREMODE_SEQUENCE] = 1;//Does bobcat have a mode like this?
captureModeTranslation[CAPTUREMODE_TRIGGER] = 1;//Hardware vs. software trigger are the same for bobcat as far as I can tell. It's a matter of whether you enable TriggerInEnable?

}

int xenics_GetCaptureMode(XCHANDLE handle) {

	long captureMode = -1;

	//Command for Bobcat camera for now, can be changed later if we need to add other cameras
	long triggerInMode = 0;
	XC_GetPropertyValueL(handle, "TriggerInMode", &triggerInMode);
	//printf("Get triggerInMode %i\n", triggerInMode);

	//Continous triggering
	if (triggerInMode == 0)//Free run
	{
		captureMode = CAPTUREMODE_CONT;
	}
	else
	{
		//If it's not continous triggering, we'll work out if it software or hardware triggered based on whether the trigger is enabled or not
		long triggerInEnable = 0;
		XC_GetPropertyValueL(handle, "TriggerInEnable", &triggerInEnable);
		if (triggerInEnable)
		{
			captureMode = CAPTUREMODE_TRIGGER;
		}
		else
		{
			captureMode = CAPTUREMODE_SINGLESHOT;
		}
	

	}
	return (int)captureMode;
}

int xenics_SetCaptureMode(XCHANDLE handle, int cMode) {
	define_captureMode();

	long captureMode = captureModeTranslation[cMode];
	XC_SetPropertyValueL(handle, "TriggerInMode", captureMode, "");
	//printf("Set TriggerInMode %i\n", captureMode);

	//Software trigger
	if (cMode == CAPTUREMODE_SINGLESHOT)
	{
		//Disable the hardware trigger
		XC_SetPropertyValueL(handle, "TriggerInEnable", 0, "");
	}

	//Hardware trigger
	if (cMode == CAPTUREMODE_TRIGGER)
	{
		//Enable the hardware trigger
		XC_SetPropertyValueL(handle, "TriggerInEnable", 1, "");
	}

	return (int)captureMode;
}


//Getwindow size
int xenicsGetWindow(XCHANDLE handle, long& woiSX, long& woiEX, long& woiSY, long& woiEY, int& w, int& h)
{
	int width, height = 0;

		// Make sure the camera is capable of setting a window of interest before continuing.
		if (I_OK == XC_GetPropertyValueL(handle, "CapWoiCount", NULL))
		{
			// Retrieve the current Woi values.
			XC_GetPropertyValueL(handle, "WoiSX(0)", &woiSX);
			XC_GetPropertyValueL(handle, "WoiEX(0)", &woiEX);
			XC_GetPropertyValueL(handle, "WoiSY(0)", &woiSY);
			XC_GetPropertyValueL(handle, "WoiEY(0)", &woiEY);

#ifdef TRANSPOSEIT
			height = XC_GetWidth(handle);
			width = XC_GetHeight(handle);
#else
			width = XC_GetWidth(handle);
			height = XC_GetHeight(handle);
#endif
			w = width;
			h = height;
			
			return 1;
		}
		else
		{
			printf("The connected camera does not support windows of interest.\n");
			return 0;
		}
	
}

int xenicsSetWindow(XCHANDLE handle, long woiSX, long woiEX, long woiSY, long woiEY)
{
	
	//printf("setting window... \t woiSX=%d\t woiEX=%d\t woiSY=%d\t woiEY=%d\n", woiSX, woiEX, woiSY, woiEY);
		// Make sure the camera is capable of setting a window of interest before continuing.
		if (I_OK == XC_GetPropertyValueL(handle, "CapWoiCount", NULL))
		{
					
			// We set a window which is half the size of the current window.
			XC_SetPropertyValueL(handle, "WoiSX(0)", woiSX, "");
			XC_SetPropertyValueL(handle, "WoiEX(0)", woiEX, "");
			XC_SetPropertyValueL(handle, "WoiSY(0)", woiSY, "");
			XC_SetPropertyValueL(handle, "WoiEY(0)", woiEY, "");
			
			return 1;
		}
		else
		{
			printf("The connected camera does not support windows of interest.\n");
			return 0;
		}
	}



Tbuff* InitPixelBuffer(XCHANDLE handle, int number_frames, unsigned short* buffer)
{
	Tbuff* buffer_structure;

	//allocate memory
	buffer_structure = (Tbuff*)calloc(1, sizeof(Tbuff));

	buffer_structure->handle = handle;
	//get the parameters (importsant for init the buffer later)
	int width, height, bitsize = 0;
	get_camera_param(handle, height, width, bitsize);

	buffer_structure->width = width; //width
	buffer_structure->height = height; //width
	buffer_structure->fullPixelCount = width * height; //frame size
	buffer_structure->pixelBufferFrameCount = number_frames; //frame count
	buffer_structure->pixel_buffer = buffer; //give the pointer to the buffer to the structure
	buffer_structure->fps = 0; //no frames measure so far (except if you have superpower), so fps=0

	size_t totalPixels = buffer_structure->pixelBufferFrameCount * buffer_structure->fullPixelCount;


	buffer_structure->pixelBufferCurrentFramePtr = buffer_structure->pixel_buffer;
	buffer_structure->pixelBufferNextFramePtr = buffer_structure->pixel_buffer;
	buffer_structure->pixelBufferCurrentFrameIdx = 0;
	buffer_structure->pixelBufferNextFrameIdx = 0;

	//buffer allocated outside C
	buffer_structure->pixelBufferEnd = &buffer[totalPixels - 1];

	//flag to start measuring frames in free run
	buffer_structure->startedFilling = false;
	//flag to start measuring frames in software trigger
	buffer_structure->software_trigger = false;

	//allocating memory for a buffer where we can discard frames
	allocate1D(buffer_structure->fullPixelCount, buffer_structure->pixelDump);
	//printf("memory allocated for the dump buffer: %p\n", buffer_structure->pixelDump);

	//counting the actual number of frames that have been recorded
	buffer_structure->FrameRecorded = 0;

	buffer_structure->captureMode = -1;

	buffer_structure->speed_loop_frame_grabbing = 0;
	buffer_structure->compt_loop_frame_grabbing = 0;

	return buffer_structure;
}

int start_frame_grabbing_thread(Tbuff* buffer_structure)
{
	//we need to start the camera using start_capture before-hand!
	
	printf("STARTING THREAD\n");

	DWORD ThreadID;
	//HANDLE  hThreadArray;
	buffer_structure->hThreadArray = CreateThread(
		NULL,                   // default security attributes
		0,                      // use default stack size  
		fill_frame_buffer_1frame,       // thread function name
		buffer_structure,          // argument to thread function 
		0,                      // use default creation flags 
		&ThreadID);   // returns the thread identifier
	

	printf("STARTED THREAD with ID %d\n", ThreadID);
	return 0;

}


int stop_frame_grabbing_thread(Tbuff* buffer_structure)
{
	buffer_structure->startedFilling = false; 
	printf("Waiting for Thread to die\n");
	jointhread(buffer_structure->hThreadArray);
	printf("Thread Dead\n");
	return 0;
}

void jointhread(HANDLE hThread2) //killing a thread
{
	WaitForSingleObject(hThread2, INFINITE);
}

DWORD WINAPI fill_frame_buffer_1frame(LPVOID param) 
{
	// define parameters
	Tbuff* buffer_structure = (Tbuff*) param;
	T2 output; //useful for using the grab frame function

	XCHANDLE handle = buffer_structure->handle;

	//init the buffer pointer 
	unsigned short* buffer = 0; 

	int fullPixelCount = buffer_structure->fullPixelCount ; //for now, no windowing

	//get the capture mode (just for the fps counter)
	buffer_structure->captureMode = xenics_GetCaptureMode(handle);

	//purge internal buffer in case we have some frames in there
	purge_internal_buffer(buffer_structure, true);

	// Start Acquisitions for this channel.
	buffer_structure->startedFilling = true;
	//fill up the frame buffer
	unsigned char newFrame = false;
	//int frame_idx = 0;
	unsigned int compt = 1;
	unsigned int compt_TOTAL = 1; //including when we don't grab frames. That's to check how fast the loop goes.
	__int64 startTime = QPC();
	__int64 startTimeInLoop = QPC();
	
	while (buffer_structure->startedFilling)
	{
		// assign the buffer 
		buffer = buffer_structure->pixelBufferNextFramePtr;
		//buffer_structure->pixelBufferCurrentFrameIdx = frame_idx; //to be removed later
		if (XC_IsCapturing(handle))
		{
			startTimeInLoop = QPC();
			output = get_frame(handle, buffer);
			newFrame = output.return_value;
			//ErrCode errorCode = XC_GetFrame(handle, FT_NATIVE, XGF_Blocking , buffer, fullPixelCount * sizeof(unsigned short)); //| XGF_NoConversion
			//newFrame = (errorCode == I_OK) && newFrame;
			

		}

		//shift the pointer 
		if (!newFrame) // get_frame returns 0 as return_value if everything went well
		{
			buffer_structure->pixelBufferCurrentFramePtr = buffer_structure->pixelBufferNextFramePtr;
			//pixelBufferCurrentFrameFloatPtr = pixelBufferNextFrameFloatPtr;
			buffer_structure->pixelBufferCurrentFrameIdx = buffer_structure->pixelBufferNextFrameIdx;

			//Next frame will be written to the next frame in the buffer
			buffer_structure->pixelBufferNextFramePtr += fullPixelCount;
			buffer_structure->FrameRecorded += 1; //We've recorded an extra frame here

			//pixelBufferNextFrameFloatPtr += totalPixelCount;

			size_t nf = (size_t)(&buffer_structure->pixelBufferNextFramePtr[fullPixelCount - 1]);
			size_t pe = (size_t)&buffer_structure->pixelBufferEnd[0];
			if (nf > pe)
			{
				buffer_structure->pixelBufferNextFramePtr = buffer_structure->pixel_buffer;
				//pixelBufferNextFrameFloatPtr = pixelBufferFloat;
				buffer_structure->pixelBufferNextFrameIdx = -1;
			}
			__int64 stopTime = QPC();
			if (buffer_structure->captureMode == CAPTUREMODE_CONT) //in continuous mode, we assume the loop runs indefinitely, so we start the clock before the loop and avaerage over the number of iterations
			{
				double eTime = QPCTimeElapsedSeconds(startTime, stopTime);
				buffer_structure->fps = (compt) / eTime;
			}
			else
			{
				double eTime = QPCTimeElapsedSeconds(startTimeInLoop, stopTime);
				buffer_structure->fps = 1 / eTime;
			}
			

			////}
			buffer_structure->pixelBufferNextFrameIdx +=1;//shifting the index of next frame by 1
			compt++;
		}

		__int64 stopTime_frameGrabbing_loop = QPC();
		double eTime_frameGrabbing_loop = QPCTimeElapsedSeconds(startTime, stopTime_frameGrabbing_loop);
		buffer_structure->speed_loop_frame_grabbing = (compt_TOTAL) / eTime_frameGrabbing_loop;
		buffer_structure->compt_loop_frame_grabbing = compt_TOTAL;
		compt_TOTAL += 1;
	}

	return 0;
}

__int64 QPC()//timer stuff
{
	LARGE_INTEGER currentTime;
	QueryPerformanceCounter(&currentTime);
	return currentTime.QuadPart;
}

double QPCTimeElapsedSeconds(__int64 startTime, __int64 stopTime) //calculate elapsed time
{
	LARGE_INTEGER Frequency;
	QueryPerformanceFrequency(&Frequency);
	unsigned __int64 dTicks = stopTime - startTime;
	double dt = (1.0 * dTicks) / Frequency.QuadPart;
	return dt;
}

int grab_from_buffer(Tbuff* buffer_structure, unsigned short* external_buffer, int frame_count) //grabbing frames from the internal buffer and storing them to an external buffer
{
	//BYO buffer!
	unsigned short* pixelBufferCurrentFramePtr = buffer_structure->pixelBufferNextFramePtr; //the current pointer pixelBufferCurrentFramePtr
	size_t pixelBufferNextFrameIdx = buffer_structure->pixelBufferNextFrameIdx; //the index of the frame we will fill next in the buffer
	size_t fullPixelCount = buffer_structure->fullPixelCount; //frame size
	//printf("fullPixelCount= %d \n", fullPixelCount);
	int return_flag = 1;
	if (frame_count>0) //we want to grab at least a frame (if the buffer stores more than a frame)
	{
		//TO ADD: check if we start at the end or at the start of the buffer, if need to grab frames from both ends...
		if (frame_count <= pixelBufferNextFrameIdx) //in this case, all the frames can be grabbed easily from the buffer (no need to grab from the tail)
		{
		unsigned short* buffer_starting = pixelBufferCurrentFramePtr - frame_count * fullPixelCount;//where to start the pointer
		memcpy(external_buffer, buffer_starting, fullPixelCount * frame_count*sizeof(short));
		printf("saved %d frames in the external buffer \n",frame_count);
		return_flag = 0;
		}
		else //we have to grab some frames from the tail first, then take frames form the start of the buffer
		{
			if (frame_count > buffer_structure->FrameRecorded) //we are trying to get frames that have not been measured
			{
				printf("%d frames requested while only %d frames measured.. shame on you\n", frame_count, buffer_structure->FrameRecorded);
			}
			else
			{
				int N_frames_tail = (frame_count - pixelBufferNextFrameIdx);
				unsigned short* buffer_starting_tail = buffer_structure->pixelBufferEnd - N_frames_tail * fullPixelCount;//where to start the pointer
				memcpy(external_buffer, buffer_starting_tail, fullPixelCount * N_frames_tail * sizeof(short)); //filling first from the tail
				memcpy(external_buffer + fullPixelCount * N_frames_tail, buffer_structure->pixel_buffer, fullPixelCount * pixelBufferNextFrameIdx * sizeof(short)); //then filling with the front from the buffer
				printf("saved %d frames in the external buffer, %d from tail \n", frame_count, N_frames_tail);
				return_flag = 0;
			}
		}
	}
	else if (frame_count == 0) //in the case of free run and we store only 1 single frame in the internal buffer, we just point to the initial position of the buffer
	{
		unsigned short* buffer_starting = buffer_structure->pixelBufferCurrentFramePtr;//where to start the pointer		
		//printf("buffer_starting %p, address = %p, frame size total=%d \n external buffer %p\n", buffer_starting, &buffer_starting, fullPixelCount, external_buffer);
		memcpy(external_buffer, buffer_starting, fullPixelCount *sizeof(short));
		return_flag = 0;
		//printf("saved 1 frame in the external buffer from the 1frame buffer\n");

	}
	else
	{
		printf("something went wrong..\n");
	}
	return return_flag;
}

//purge the internal buffer
int xenics_PurgeInternalBuffer(Tbuff* buffer_structure)
{	
	XCHANDLE handle = buffer_structure->handle;
	unsigned short* pixelDump = buffer_structure->pixelDump;
	size_t fullpixelCount = buffer_structure->fullPixelCount;
	ErrCode errorCode = I_OK;
	//The number of frames that were purged from the buffer
	int purgeCount = 0;
	//The number of times we've requested a frame from the buffer, and there isn't one there.
	int noFrameCount = 0;
	//The number of times we want to get a response of 'no frame' from the camera, before we're satisfied that there truly is no frames left in the buffer.
	//This extra check shouldn't be necessary.
	const int noFrameCountGoal = 3;

	//Keep requesting frames from the camera, until there's no frames left.
	while (noFrameCount < noFrameCountGoal)
	{
		errorCode = XC_GetFrame(handle, FT_NATIVE, XGF_NoConversion, pixelDump, fullpixelCount * sizeof(unsigned short));

		if (errorCode == I_OK)
		{
			purgeCount++;
		}
		else
		{
			if (errorCode == E_NO_FRAME)
			{
				//This pause should not be necessary, but I think we need to wait at least 1 frame duration to make sure there truly is no frames.
				//Perhaps there is a frame being internally processed, that is not yet ready, but is still from the past.
				//Adding this 32ms sleep here made the purge significantly more reliable. At least on the xeva cameras.
				Sleep(32);
				noFrameCount++;
			}
			else
			{
				//You shouldn't end up here.
				printf("PurgeInternalBuffer error.\n");
			}
		}
	}
	return purgeCount;
}

void purge_internal_buffer(Tbuff* buffer_structure, bool print_outcome)
{
		//purge the internal buffer in case there are frames there
	int purgeCount = xenics_PurgeInternalBuffer(buffer_structure);
	if (print_outcome)
	{
		printf("Purged internal buffer of %d frames\n", purgeCount);
	}	
	//return purgeCount;
}

//software trigger
int xenicsSoftwareTrigger(XCHANDLE handle)
{
	//SoftwareTrigger//Bobcat (Bobcat 320 GigE-Vision - F03A )
	XC_SetPropertyValueL(handle, "SoftwareTrigger", 1, "");
	return 0;

}

int reset_buffer(Tbuff* buffer_structure, int frame_idx, bool purge_buffer)
{
	if (frame_idx < 0) //if the start of the buffer is negative, then you should be ashamed 
	{
		frame_idx = 0;
	}
	
	if (purge_buffer)
	{
		//purge internal buffer in case we have some frames in there
		purge_internal_buffer(buffer_structure,true);
	}

	//setting the current frame to the desired position
	size_t offset = frame_idx * buffer_structure->fullPixelCount;
	buffer_structure->pixelBufferCurrentFrameIdx = frame_idx;
	buffer_structure->pixelBufferNextFrameIdx = frame_idx;
	buffer_structure->pixelBufferNextFramePtr = &buffer_structure->pixel_buffer[offset];
	buffer_structure->FrameRecorded = 0; //restart the frame counter too

	//pixelBufferNextFrameFloatPtr = &pixelBufferFloat[offset];
	return 0;
}

//we assume here that the camera is already in software trigger mode. We will launch the software trigger and save the frame
int SoftwareTrigger(Tbuff* buffer_structure)
{
	//first, if we were not in software trigger before, we need to reset the buffer position
	if (!buffer_structure->software_trigger)
	{
		// checking if we stopped the thread to grab frames in free run
		if (buffer_structure->startedFilling)
		{
			stop_frame_grabbing_thread(buffer_structure); //kill the thread and stop free run acquisition
		}

		int response = reset_buffer(buffer_structure, 0,true);//resetting the buffer to 0
		if (response == 0)
		{
			printf("buffer reset!\n");
		}
		//set the buffer software trigger flag to True (if it wasn't already)
		buffer_structure->software_trigger = true;
	}

	//software trigger and store the frame in the buffer
	T2 output;
	XCHANDLE handle = buffer_structure->handle;
	unsigned short* buffer = 0;
	unsigned char newFrame = false;
	int fullPixelCount = buffer_structure->fullPixelCount; //for now, no windowing

	__int64 startTime = QPC();


	xenicsSoftwareTrigger(handle);//software trigger
	buffer = buffer_structure->pixelBufferNextFramePtr;
	if (XC_IsCapturing(handle))
	{
		output = get_frame(handle, buffer); //get the frme and store it in the buffer
		newFrame = output.return_value;
	}

	//shift the pointer now
	if (!newFrame)//if we actually measure a frame, which we should
	{
		buffer_structure->pixelBufferCurrentFramePtr = buffer_structure->pixelBufferNextFramePtr;
		//pixelBufferCurrentFrameFloatPtr = pixelBufferNextFrameFloatPtr;
		buffer_structure->pixelBufferCurrentFrameIdx = buffer_structure->pixelBufferNextFrameIdx;

		//Next frame will be written to the next frame in the buffer
		buffer_structure->pixelBufferNextFramePtr += fullPixelCount;
		//pixelBufferNextFrameFloatPtr += totalPixelCount;

		size_t nf = (size_t)(&buffer_structure->pixelBufferNextFramePtr[fullPixelCount - 1]);
		size_t pe = (size_t)&buffer_structure->pixelBufferEnd[0];
		if (nf > pe)//in case the last frame is outside the range of the current buffer, we wrap it up and next frame is at the start. Quite unlikely though
		{
			buffer_structure->pixelBufferNextFramePtr = buffer_structure->pixel_buffer;
			//pixelBufferNextFrameFloatPtr = pixelBufferFloat;
			buffer_structure->pixelBufferNextFrameIdx = -1;
		}
		__int64 stopTime = QPC();
		double eTime = QPCTimeElapsedSeconds(startTime, stopTime);
		buffer_structure->fps = (1) / eTime; //time to process the frame, not the time between frames

		////}
		buffer_structure->pixelBufferNextFrameIdx += 1;//shifting the index of next frame by 1


	}

	return 0;
}

int get_trigger_polarity(XCHANDLE handle)
{
	//0: low level / falling edge
	//1: high level/ rising edge

	long polarity = 0;
	XC_GetPropertyValueL(handle, "TriggerInPolarity", &polarity);
	return (int)polarity;

}

int set_trigger_polarity(XCHANDLE handle, long polarity_mode)
{
	//0: low level / falling edge
	//1: high level/ rising edge
	
	XC_SetPropertyValueL(handle, "TriggerInPolarity", polarity_mode,"");
	return (int)polarity_mode;

}

void main() {
	int a = 5;
	long b = (long)a;
	printf("a=%d\t b=%d", a, b);
	printf("a=%d\t b=%d", a, b);
}