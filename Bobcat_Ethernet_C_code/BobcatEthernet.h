#pragma once

#include <iostream>
#include "XCamera.h"
#include "XFilters.h"

#define WIN32
#ifdef WIN32

#include <windows.h>

//BOOL WINAPI consoleHandler(DWORD signal);
//
//void jointhread(HANDLE hThread2);
//
//typedef HANDLE nThread;

#endif

const int error_str_size = 1024; // I think this is the maximum error string size but I cannot find it in Xenics SDK

typedef struct
{
	unsigned int device_count;
	ErrCode errorCode;
	char error_str[1024];
} T0;

typedef struct
{
	XDeviceInformation devices[10]; //We will never have more than 10 Xenics cameras on one system I hope:) I can't figure out a better way to do this than hard-coding!
	ErrCode errorCode;
	char error_str[1024];
} T1;

typedef struct
{
	int return_value;
	ErrCode errorCode;
	char error_str[1024];
} T2;

typedef struct
{
	double exposure_time;
	ErrCode errorCode;
	char error_str[1024];
} T3;

typedef struct
{
	ErrCode errorCode;
	char error_str[1024];
} XErr;

typedef struct
{
	unsigned long software_correction_filterID;
	bool filter_running;
	int return_value;
} T4;

typedef struct
{
	XCHANDLE handle;
	unsigned short* pixel_buffer;//buffer where w e store the frames from the camera
	size_t pixelBufferFrameCount; //number of frames we store
	size_t fullPixelCount;// frame size
	int width;
	int height;
	double fps;
	unsigned short* pixelBufferEnd;
	unsigned short* pixelBufferCurrentFramePtr;
	unsigned short* pixelBufferNextFramePtr;
	size_t pixelBufferCurrentFrameIdx;
	size_t pixelBufferNextFrameIdx;

	bool startedFilling;

	HANDLE  hThreadArray; //saves the thread in there

	unsigned short* pixelDump; 

	bool software_trigger;
	int FrameRecorded;
	int captureMode;

	double speed_loop_frame_grabbing;
	double compt_loop_frame_grabbing;

}Tbuff;


// utility functions
extern "C" __declspec(dllexport) T0 enumerate_all_connected_xenics_cameras();
extern "C" __declspec(dllexport) T1 print_connected_cameras_info(unsigned int deviceCount);

// open/close connection to a camera
extern "C" __declspec(dllexport) XCHANDLE init_camera(char* URL);
extern "C" __declspec(dllexport) int close_camera(XCHANDLE handle);

// Start/Stop capture
extern "C" __declspec(dllexport) T2 start_capture(XCHANDLE handle);
extern "C" __declspec(dllexport) T2 stop_capture(XCHANDLE handle);


// Get frame
extern "C" __declspec(dllexport) T2 get_frame(XCHANDLE handle, unsigned short* frame);

// Camera Settings

extern "C" __declspec(dllexport) T3 get_exposure_time(XCHANDLE handle);
extern "C" __declspec(dllexport) XErr set_exposure_time(XCHANDLE handle, double exposure_time_in_us);

extern "C" __declspec(dllexport) T4 turn_on_software_correct(XCHANDLE handle, const char* packpath);

// Get/Set trigger mode
extern "C" __declspec(dllexport) int xenics_GetCaptureMode(XCHANDLE handle);
extern "C" __declspec(dllexport) int xenics_SetCaptureMode(XCHANDLE handle, int cMode);

//Get parameters form the camera (width, height, bitsize)
extern "C" __declspec(dllexport) void get_camera_param(XCHANDLE handle, int& height, int& width, int& bitsize);

//Get/Set Window of Interest
extern "C" __declspec(dllexport) int xenicsGetWindow(XCHANDLE handle, long& woiSX, long& woiEX, long& woiSY, long& woiEY, int& w, int& h);
extern "C" __declspec(dllexport) int xenicsSetWindow(XCHANDLE handle, long woiSX, long woiEX, long woiSY, long woiEY);

//setting up the buffer, start/stop saving frames in the buffer and grab frames from the buffer
extern "C" __declspec(dllexport) Tbuff * InitPixelBuffer(XCHANDLE handle, int number_frames, unsigned short* buffer);
DWORD WINAPI fill_frame_buffer_1frame(LPVOID param);
void jointhread(HANDLE hThread2);
__int64 QPC();
double QPCTimeElapsedSeconds(__int64 startTime, __int64 stopTime);
void purge_internal_buffer(Tbuff* buffer_structure, bool print_outcome);

extern "C" __declspec(dllexport) int start_frame_grabbing_thread(Tbuff * buffer_structure);
extern "C" __declspec(dllexport) int stop_frame_grabbing_thread(Tbuff * buffer_structure);
extern "C" __declspec(dllexport) int grab_from_buffer(Tbuff * buffer_structure, unsigned short* external_buffer, int frame_count);

//purging the internal buffer
extern "C" __declspec(dllexport) int xenics_PurgeInternalBuffer(Tbuff* buffer_structure);
//extern "C" __declspec(dllexport) int xenicsSoftwareTrigger(XCHANDLE handle);

extern "C" __declspec(dllexport) int SoftwareTrigger(Tbuff* buffer_structure);

//reset the frame buffer
extern "C" __declspec(dllexport) int reset_buffer(Tbuff * buffer_structure, int frame_idx, bool purge_buffer);

//get/set camera trigger polarity
extern "C" __declspec(dllexport)  int get_trigger_polarity(XCHANDLE handle);
extern "C" __declspec(dllexport)  int set_trigger_polarity(XCHANDLE handle, long polarity_mode);