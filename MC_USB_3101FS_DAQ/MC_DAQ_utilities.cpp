#include "MC_DAQ_utlities.h"

unsigned short convert_value_from_eng_to_board_units(int boardnum, int range, float value_eng_units) {

	unsigned short value_board_units;

	//unsigned short* array_in_board_units = (unsigned short*) malloc(array_length * sizeof(unsigned short));
	/*for (int i = 0; i < array_length; ++i) {
		
	}*/

	cbFromEngUnits(boardnum, range, value_eng_units, &value_board_units);
	printf("Function run correctly.\n");

	return value_board_units;
	
	//return array_in_board_units;
};


unsigned short* convert_array_from_eng_to_board_units(int boardnum, int range, float* array_eng_units, int array_length) {
	
	unsigned short* array_board_units = (unsigned short*)malloc(array_length * sizeof(unsigned short));
	for (int i = 0; i < array_length; ++i) {
		
		cbFromEngUnits(boardnum, range, array_eng_units[i], &array_board_units[i]);
	}

	printf("The converted value is: %u \n", array_board_units[0]);

	return array_board_units;
};