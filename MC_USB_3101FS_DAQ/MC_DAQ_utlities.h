#pragma once

#include <iostream>
#include "cbw.h"

extern "C" __declspec(dllexport) unsigned short convert_value_from_eng_to_board_units(int boardnum, int range, float value_eng_units);
extern "C" __declspec(dllexport) unsigned short* convert_array_from_eng_to_board_units(int boardnum, int range, float* array_eng_units, int array_length);