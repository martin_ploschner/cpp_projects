
//INCASE YOU WANT TO COMPILE THIS INTO A DLL
//cl.exe /LD testGrab.cpp multicam.lib /I"C:/Program Files (x86)/Euresys/MultiCam/Include" -link -LIBPATH:"C:/Program Files (x86)/Euresys/MultiCam/Lib/"
//cl.exe /LD testGrab.cpp multicam.lib /I"C:/Program Files (x86)/Euresys/MultiCam/Include" -link -LIBPATH:"C:/Program Files (x86)/Euresys/MultiCam/Lib/amd64"
#include <stdio.h>
#include <stdlib.h>

#include "euresysnick.h" 


/*
Need to open two channel for the thing to work....
The channels are configured by the cam files.
*/
int OpenDualChannel(MCHANDLE* hC1, MCHANDLE* hC2, int Board1, int Board2, char* camfile1, char* camfile2)
{
	MCSTATUS status = MC_OK;
	MCHANDLE& hChannel1 = *hC1;
	MCHANDLE& hChannel2 = *hC2;

	// MultiCam Open Driver
	status = McOpenDriver(NULL);
	if (status != MC_OK) goto Finalize;

	// MCHANDLE hChannel1;
	// MCHANDLE hChannel2;

	//  Create a channel.
	status = McCreate(MC_CHANNEL, &hChannel1);
	if (status != MC_OK) goto Finalize;
	status = McCreate(MC_CHANNEL, &hChannel2);
	if (status != MC_OK) goto Finalize;

	

	// Link the channel to a board. Here we take the first board.
	status = McSetParamInt(hChannel1, MC_DriverIndex, Board1);
	if (status != MC_OK) goto Finalize;
    
    status = McSetParamInt(hChannel2, MC_DriverIndex, Board2);
	if (status != MC_OK) goto Finalize;
    
	status = McSetParamStr(hChannel1, MC_Connector, "M");
	status = McSetParamStr(hChannel2, MC_Connector, "M");

	if (camfile1[0] != 0)
	{
		printf("opening Channel 1");
		printf("%s\n",camfile1);
		status = McSetParamStr(hChannel1, MC_CamFile, camfile1);
		if (status != MC_OK) goto Finalize;
	}

	if (camfile2[0] != 0)
	{
		printf("opening Channel 2");
		printf("%s\n",camfile2);
		status = McSetParamStr(hChannel2, MC_CamFile, camfile2);
		if (status != MC_OK) goto Finalize;
	}

	return status;

	Finalize:
	if (status != MC_OK)
	{
		printf("InitChannel Error %d\n",status);
	}

	if (hChannel1)
	{
		McDelete(hChannel1);
	}
	
	if (hChannel2)
	{
		McDelete(hChannel2);
	}

	return status;

}



int setupAQ(MCHANDLE hChannel)
{
    MCSTATUS status = MC_OK;
	// Set the acquisition mode
	McSetParamInt(hChannel, MC_AcquisitionMode, MC_AcquisitionMode_SNAPSHOT); //MC_AcquisitionMode_HFR
	if (status != MC_OK) goto Finalize;

	// Choose the number of lines per page
	McSetParamInt(hChannel, MC_SeqLength_Fr, MC_INDETERMINATE);
	if (status != MC_OK) goto Finalize;

	// Choose the way the first acquisition is triggered
	McSetParamInt(hChannel, MC_TrigMode, MC_TrigMode_SOFT);
	if (status != MC_OK) goto Finalize;

	// Choose the triggering mode for subsequent acquisitions
	McSetParamInt(hChannel, MC_NextTrigMode, MC_NextTrigMode_REPEAT);
	if (status != MC_OK) goto Finalize;

	McSetParamInt(hChannel,MC_ExposeOverlap,MC_ExposeOverlap_ALLOW);

	status = McSetParamInt(hChannel,MC_AcqTimeout_ms,-1);


    status = McSetParamInt(hChannel, MC_SignalEnable + MC_SIG_SURFACE_PROCESSING, MC_SignalEnable_ON);
	if (status != MC_OK) goto Finalize;

	status = McSetParamInt(hChannel, MC_SignalEnable + MC_SIG_ACQUISITION_FAILURE, MC_SignalEnable_ON);
	if (status != MC_OK) goto Finalize;

	status = McSetParamInt(hChannel, MC_SignalEnable + MC_SIG_CLUSTER_UNAVAILABLE, MC_SignalEnable_ON);
	if (status != MC_OK) goto Finalize;

    return 0;
    Finalize:
    return 1;


    
}

int initChannelSurface(MCHANDLE hChannel,tSurfaceInfo* ptThr)
{
    MCSTATUS status = MC_OK;
	// Retrieve channel size information.
	status = McGetParamInt(hChannel, MC_ImageSizeX, &ptThr->width);
	if (status != MC_OK) goto Finalize;
	status= McGetParamInt(hChannel, MC_ImageSizeY, &ptThr->height);
	if (status != MC_OK) goto Finalize;  
	status= McGetParamInt(hChannel, MC_BufferPitch, &ptThr->pitch);
	if (status != MC_OK) goto Finalize;  

	McSetParamInt(hChannel, MC_SurfaceCount, 256);
	McSetParamInt(hChannel,MC_MaxFillingSurfaces,MC_MaxFillingSurfaces_MAXIMUM);

    printf("%d,%d,%d",ptThr->width,ptThr->height,ptThr->pitch);

    return 0;
    Finalize:
        return 1;
	
}

//  Create a channel and set its parameters
int PrepareDualChannel(MCHANDLE hChannel1,MCHANDLE hChannel2, tThrDual** ppsPhxSnap)
{
	MCSTATUS status = MC_OK;
    tPhxSnap* psPhxSnap;
    tThrDual* ptThr;
   
    psPhxSnap = (tPhxSnap *) calloc(1,sizeof(tPhxSnap)    );
    ptThr     = (tThrDual *) calloc(1, sizeof(tThrDual)    );

    setupAQ(hChannel1);
    setupAQ(hChannel2);
	//McSetParamInt(hChannel2, MC_TrigMode, MC_TrigMode_SOFT);

    initChannelSurface(hChannel1, &ptThr->surface1);
    initChannelSurface(hChannel2, &ptThr->surface2);

	psPhxSnap->running = FALSE;
    ptThr->psPhxSnap   = psPhxSnap;
    ptThr->hChannel1     = hChannel1;
    ptThr->hChannel2     = hChannel2;
    ptThr->dwGrabbed    = 0;
    ptThr->dwAcquired   = 0;
    ptThr->fLastGrabbed  = 0;

	*ppsPhxSnap  = ptThr; ///Set the pointer handle
    

	return 0;

}


// Start the image acquisition, images are processed in the callback
int startAQDual(tThrDual *psPhxSnap, int numBuffers) 
{
	MCHANDLE hChannel1 = psPhxSnap->hChannel1;
    MCHANDLE hChannel2 = psPhxSnap->hChannel2;
	MCSTATUS status = 0;

	//MAYBE WE MAKE ALL THE BUFFERS HERE?
	
   tPhxSnap* pCont = psPhxSnap->psPhxSnap;
   

   ui32 dwlastbuffer = 0;
   ui32 dwBuffersAvailable = 0;
   
   ui8* buffer;
   ui32 dwPayloadSize = psPhxSnap->surface1.pitch*psPhxSnap->surface1.height;

   psPhxSnap->numBuffers = numBuffers;

   int buffersReady;
   long long* timestamps = (long long*) malloc(psPhxSnap->numBuffers*sizeof(long long)*2);
   int* ipFrameCounters = (int*) malloc(psPhxSnap->numBuffers*sizeof(int)*2);
   
   //make a bunch of buffers
   psPhxSnap->bufferidx = 0;
   ui8* buffers =  (ui8*) malloc( psPhxSnap->numBuffers*dwPayloadSize*2 );  // <----  twice as big as it needs to be!!! 

   psPhxSnap->buffers          = buffers;
   psPhxSnap->timestamps       = timestamps;
   psPhxSnap->ipFrameCounters  = ipFrameCounters;

   printf("STARTING THREAD\n");

   DWORD ThreadID;
   psPhxSnap->pThread = (void *)CreateThread( 
            NULL,                   // default security attributes
            0,                      // use default stack size  
            GrabDualThreadProc,       // thread function name
            psPhxSnap,          // argument to thread function 
            0,                      // use default creation flags 
            &ThreadID);   // returns the thread identifier

	printf("STARTED THREAD with ID %d\n",ThreadID);
    //GrabDualThreadProc(psPhxSnap);



	return 0;

	

Finalize:
	if (status!=MC_OK)
	{
		pCont->running = FALSE;
		printf("Acquire Error %d\n",status);
	}

	if (hChannel1)
	{
		McDelete(hChannel1);
	}

    if (hChannel2)
	{
		McDelete(hChannel2);
	}
}

int stopAQDual(tThrDual *psPhxSnap)
{
	MCHANDLE hChannel1 = psPhxSnap->hChannel1;
    MCHANDLE hChannel2 = psPhxSnap->hChannel2;
	MCSTATUS status = 0;

	psPhxSnap->psPhxSnap->running = false;
	status= McSetParamInt(hChannel1, MC_ChannelState, MC_ChannelState_IDLE);
    status= McSetParamInt(hChannel2, MC_ChannelState, MC_ChannelState_IDLE);

	printf("Waiting for Thread to die\n");
	jointhread(psPhxSnap->pThread);
	printf("Thread Dead\n");
	//FREEEDOM!!!
   free(psPhxSnap->buffers);
   free(psPhxSnap->timestamps);
   free(psPhxSnap->ipFrameCounters);
   return 0;

}

int  WaitForSignal(MCSIGNAL signal, MCHANDLE hChannel, UINT32 timeout,
                                             PMCSIGNALINFO cSignalInfo)
{

	MCSTATUS status = 0;
    memset(cSignalInfo, 0, sizeof(cSignalInfo));
    // CALLBACK REASONS
// #define MC_MAX_EVENTS                       12
// #define MC_SIG_ANY                          0
// #define MC_SIG_SURFACE_PROCESSING           1
// #define MC_SIG_SURFACE_FILLED               2
// #define MC_SIG_UNRECOVERABLE_OVERRUN        3
// #define MC_SIG_FRAMETRIGGER_VIOLATION       4
// #define MC_SIG_START_EXPOSURE               5
// #define MC_SIG_END_EXPOSURE                 6
// #define MC_SIG_ACQUISITION_FAILURE          7
// #define MC_SIG_CLUSTER_UNAVAILABLE          8
// #define MC_SIG_RELEASE                      9
// #define MC_SIG_END_ACQUISITION_SEQUENCE     10
// #define MC_SIG_START_ACQUISITION_SEQUENCE   11
// #define MC_SIG_END_CHANNEL_ACTIVITY         12
    status = McWaitSignal(hChannel, signal, timeout, cSignalInfo);

	if (status == 0)
	{
		return cSignalInfo->Signal;
	}
    return -1000;
}




DWORD WINAPI GrabDualThreadProc(LPVOID Param)
{
    MCSTATUS status = 0;
	int status2 = 0 ;
	long long timestamp1;
	long long timestamp2;
	long long *timestamp;

	long long lastfpstime;

	int c1frame = 0;
	int c2frame = 0;

	int *cframe;
	int lastframe;

	int c1ActCount = 0;
	int c2ActCount = 0;

	int *cActCount;

    tThrDual *psPhxSnap = (tThrDual *) Param;
    tPhxSnap* pCont = psPhxSnap->psPhxSnap;

	MCHANDLE hSurface;
	BYTE *pImage;
    
    MCHANDLE hChannel1 = psPhxSnap->hChannel1;
    MCHANDLE hChannel2 = psPhxSnap->hChannel2;

	MCHANDLE hChannel ; // Use this for simplified loop

	MCSIGNALINFO cSignalInfo;

    // Start Acquisitions for this channel.
	pCont->running = TRUE; 
	status = McSetParamInt(psPhxSnap->hChannel1, MC_ChannelState, MC_ChannelState_ACTIVE);
	if (status != MC_OK) stopAQDual(psPhxSnap);
	status = McSetParamInt(psPhxSnap->hChannel2, MC_ChannelState, MC_ChannelState_ACTIVE);
	if (status != MC_OK) stopAQDual(psPhxSnap);

	//Assuming these take some time to start...
	

	status = McSetParamInt(psPhxSnap->hChannel1, MC_ForceTrig, MC_ForceTrig_TRIG);
	if (status != MC_OK) stopAQDual(psPhxSnap);
	status = McSetParamInt(psPhxSnap->hChannel2, MC_ForceTrig, MC_ForceTrig_TRIG);
	if (status != MC_OK) stopAQDual(psPhxSnap);


    //pDoc->m_GrablinkDualFullGrabber->StartAcquisition(-1);
	printf("INSIDE THE THREAD, about to start looping\n");
	cframe = &c1frame;
	while (pCont->running) {
	   
	   //Could do it this way.
	   if (c2frame>=c1frame)
	   {
		   hChannel = hChannel1;
		   cframe   = &c1frame;
		   cActCount = &c1ActCount;
		   timestamp = &timestamp1;
	   }
	   else
	   {
		   hChannel = hChannel2;
		   cframe = &c2frame;   
		   cActCount = &c2ActCount;
		   timestamp = &timestamp2;
	   }

		// Get Signal Information
	    status2 = WaitForSignal(MC_SIG_ANY, hChannel, 0, &cSignalInfo);
       
		if (status2 == MC_SIG_SURFACE_PROCESSING)
		{

			

			hSurface = cSignalInfo.SignalInfo;
			McSetParamInt(hSurface, MC_SurfaceState, MC_SurfaceState_FREE);
			McGetParamInt64(hSurface, MC_TimeStamp_us, timestamp);

			lastframe = *cframe;
			McGetParamInt(hSurface, MC_TimeCode, cframe);

			int bufferidx = *cframe % psPhxSnap->numBuffers;
			const int offset = (hChannel == hChannel2);


			if ((*cframe-lastframe)>1)
			{
				printf("DROPPED %d FRAMES ON CHANNEL %d\n", *cframe-lastframe-1,offset);
			}

			McGetParamPtr(hSurface,MC_SurfaceAddr,(PVOID*)&pImage);
			//cframe shoudl be the bufferidx...
			loadUpBufferDual(psPhxSnap, pImage, bufferidx, offset);
			

			
			psPhxSnap->timestamps[2*bufferidx      + offset] = *timestamp;// getMicrotime();
			psPhxSnap->ipFrameCounters[2*bufferidx + offset] = *cframe;

			psPhxSnap->psPhxSnap->fBufferGrabbed = min(c2frame,c1frame);



			if ( (bufferidx) % 61 ==20)
			{
				//calculate the fps
				psPhxSnap->fps =(double) (20000000/( (double) (psPhxSnap->timestamps[2*bufferidx]-psPhxSnap->timestamps[2*(bufferidx-20)])));
			}






		}
		else if (status2 == MC_SIG_CLUSTER_UNAVAILABLE)
		{
			//(*cActCount)++;
			//indicates a frame is grabbed...
		}


       

	}

    return 0; 
}


void loadUpBufferDual(tThrDual *psPhxSnap, BYTE *buffer, int bufferidx,int right)
{
   //Don't need to implement this one until the cheeettAAAH

   tPhxSnap* pCont = psPhxSnap->psPhxSnap;

   ui32 dwPayloadSize = psPhxSnap->surface1.pitch*psPhxSnap->surface1.height;

   long long* timestamps =psPhxSnap->timestamps;// malloc(psPhxSnap->numBuffers*sizeof(long long));
   int* ipFrameCounters = psPhxSnap->ipFrameCounters;//malloc(psPhxSnap->numBuffers*sizeof(int));
   ui8* buffers = psPhxSnap->buffers;//# malloc( psPhxSnap->numBuffers*dwPayloadSize );

//    psPhxSnap->buffers    = buffers;
//    psPhxSnap->timestamps = timestamps;
//    psPhxSnap->ipFrameCounters = ipFrameCounters;
   //should keep track of frame timing also....

	

	
	//PROTECT US FROM the grab async!!! 
	AquireApiLock();
	
	//memcpy(buffers + bufferidx*dwPayloadSize*2+right*dwPayloadSize, buffer,  dwPayloadSize);
	ui8* buffertofill= (buffers + bufferidx*dwPayloadSize*2 + right*8);
	int i=0;
	for(; i<dwPayloadSize; i+=8, buffertofill+=16, buffer+=8)
	{
		buffertofill[0] = buffer[0];
		buffertofill[1] = buffer[1];
		buffertofill[2] = buffer[2];
		buffertofill[3] = buffer[3];
		buffertofill[4] = buffer[4];
		buffertofill[5] = buffer[5];
		buffertofill[6] = buffer[6];
		buffertofill[7] = buffer[7];
	}


	ReleaseApiLock();

   }

int framesReadyDual(tThrDual *psPhxSnap)
{
   ui32 framesReady = psPhxSnap->psPhxSnap->fBufferGrabbed - psPhxSnap->fLastGrabbed;
  // printf("fBufferGrabber,fLastGrabbed %d  %d\n",psPhxSnap->psPhxSnap->fBufferGrabbed,  psPhxSnap->fLastGrabbed);
   if (framesReady>psPhxSnap->numBuffers)
   {
      framesReady = psPhxSnap->numBuffers;
   }

   return framesReady;

}
   
int grabFromBufferAsyncDual(tThrDual *psPhxSnap, void *pBlock, long long* plTimeStamps, int* fFrameCounters, int frames,  int grabtail)
{
   //provide your own buffer! 
   //tail means that you fill just grab the last N frames regardless of how many are in the bufferZ

   tPhxSnap* pCont = psPhxSnap->psPhxSnap;
   ui32 dwPayloadSize = psPhxSnap->surface1.pitch*psPhxSnap->surface1.height*2;
   ui32 returnFlag = 0;

   if (pBlock ==0)
   {
      //Intend to allocate the array here....
   }


   //BLOCK THE API WHILE WE DO A COPY....
   AquireApiLock();
   // JUst grab the last
   if (  ( framesReadyDual(psPhxSnap) >= frames)  || grabtail>0 )
    {
      //printf("")
	  psPhxSnap->bufferidx = pCont->fBufferGrabbed %  psPhxSnap->numBuffers;
      if ( psPhxSnap->bufferidx >= frames   )
      {

         //Single memcpy
         memcpy(pBlock,   psPhxSnap->buffers +(psPhxSnap->bufferidx-frames)*dwPayloadSize,frames*dwPayloadSize   );

         memcpy(plTimeStamps,  psPhxSnap->timestamps        +   2*(psPhxSnap->bufferidx-frames),        2*frames*sizeof(long long)   );

         memcpy(fFrameCounters, psPhxSnap->ipFrameCounters  +   2*(psPhxSnap->bufferidx-frames),        2*frames*sizeof(int)   );
         returnFlag = 1;
      }
      else
      {
         //printf("LOOP\n");
         ui32 tail  = frames - psPhxSnap->bufferidx; //how many to grab from the end and put in the front
         ui32 front = frames - tail; 

         memcpy(pBlock,                      psPhxSnap->buffers+(psPhxSnap->numBuffers-tail)*dwPayloadSize, tail*dwPayloadSize   );
         memcpy(((char*) pBlock) + tail*dwPayloadSize,   psPhxSnap->buffers,                                           front*dwPayloadSize   );

         memcpy(plTimeStamps, psPhxSnap->timestamps  +  2*(psPhxSnap->numBuffers-tail) , 2*tail*sizeof(long long)   );
         memcpy(plTimeStamps+tail, psPhxSnap->timestamps,                                           2*front*sizeof(long long)   );

         //Copy TimeStamp Information
         memcpy(fFrameCounters,                      psPhxSnap->ipFrameCounters  + 2*(psPhxSnap->numBuffers - tail)  ,  2*tail*sizeof(int)   );
         memcpy(fFrameCounters+tail ,                psPhxSnap->ipFrameCounters,    2*front*sizeof(int)   );
         returnFlag = 1;
         /* code */
      }
      
      if (grabtail==0)
      {
         //Only reset the frame counter if tail >
         psPhxSnap->fLastGrabbed = pCont->fBufferGrabbed;
      }


	  //Check if frame counters consistent...
	  //fFrameCounters

   }
   else
   {

   }
   ReleaseApiLock();

   return returnFlag;


}


void getstatsdual(tThrDual *psPhxSnap, double* fps, ui32* lastgrabbed )
{
   //return psPhxSnap->fps;
   *fps = psPhxSnap->fps;
   *lastgrabbed = psPhxSnap->psPhxSnap->fBufferGrabbed;
}