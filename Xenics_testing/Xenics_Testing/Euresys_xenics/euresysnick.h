
#ifdef _WIN32
#define WIN32
#endif


#include <multicam.h>

//#ifndef BYTE
//#define BYTE unsigned char
//#endif

typedef unsigned char ui8;
typedef unsigned int ui32;
typedef int BOOL;


#ifdef WIN32

#include <windows.h>

BOOL WINAPI consoleHandler( DWORD signal);

void jointhread(HANDLE hThread2);

typedef HANDLE nThread;

#endif

//TECHNICALLY WE CAN COMPILE AS C OR C++... I like C!!!
#ifdef __cplusplus
#define EXPORT extern "C" __declspec(dllexport)
#else
#define EXPORT __declspec(dllexport)
#endif

BOOL  AquireApiLock();
BOOL ReleaseApiLock();


//typedef tHandle


/* Define an application specific structure to hold user information */
typedef struct
{
   /* Control Flags */
    BOOL fBufferReady;
    int fBufferCount;
    int fFrameCount;
    int fBufferGrabbed;
    long long lBufferTime;
    BOOL running;
} tPhxSnap;


typedef struct
{
   /* data */
   
   ui8* buffers;
   long long* timestamps; //Timestamps
   int* ipFrameCounters; //Timestamps

   double fps; // keeptrack if we want...
   
   ui32 numBuffers; /*Set once*/
   ui32 bufferidx;
   ui32 dwAcquired;
   ui32 dwGrabbed;

   ui32 fLastGrabbed;

   /*Keep the context*/
   tPhxSnap* psPhxSnap; /* does not change*/

   int width;
   int height;
   int pitch;


   MCHANDLE hCamera;  /*does not change*/
} tThr;

typedef struct
{
   int width;
   int height;
   int pitch;
} tSurfaceInfo;
 
typedef struct
{
   /* data */
   
   ui8* buffers;
   long long* timestamps; //Timestamps
   int* ipFrameCounters; //Timestamps

   double fps; // keeptrack if we want...
   
   ui32 numBuffers; /*Set once*/
   ui32 bufferidx;
   ui32 dwAcquired;
   ui32 dwGrabbed;

   ui32 fLastGrabbed;

   /*Keep the context*/
   tPhxSnap* psPhxSnap; /* does not change*/

   tSurfaceInfo surface1;
   tSurfaceInfo surface2;

   void* pThread;

   MCHANDLE hChannel1;  /*does not change*/
   MCHANDLE hChannel2;  /*does not change*/
} tThrDual;



void loadUpBuffer(tThr *psPhxSnap,BYTE *buffer);
//long long getMicrotime();
//int initDriver();
//int closeDriver()
//CFFI EXPORTS!!!!!!!!!!
//EXPORT MCHANDLE;
EXPORT int OpenChannelSingle(MCHANDLE* hC,int Board, char* camfile);
EXPORT int initDriver();
EXPORT long long getMicrotime();
EXPORT int PrepareSingleChannel(MCHANDLE hChannel, tThr** ppsPhxSnap) ;

EXPORT int stopAQ(tThr *psPhxSnap);
EXPORT int startAQ(tThr *psPhxSnap, int numBuffers) ;
EXPORT void closeDriver();
EXPORT void status(tThr *psPhxSnap);
EXPORT int grabFromBufferAsync(tThr *psPhxSnap, void *pBlock, long long* plTimeStamps, int* fFrameCounters, int frames,  int grabtail);
EXPORT int OpenSerial(int board,char* port);
EXPORT int setupROI(MCHANDLE hChannel, int width, int height);
EXPORT void getstats(tThr *psPhxSnap, double* fps, ui32* lastgrabbed );
EXPORT int framesReady(tThr *psPhxSnap);
//EXPORT int softTriggerCC1(int board,MCHANDLE hC);

//Triggering stuff
EXPORT int softTriggerCC1(MCHANDLE hChannel);
EXPORT int ExternalTrigger(MCHANDLE hChannel);



//DUAL CHANNEL STUFF
//EXPORT int OpenDualChannel(MCHANDLE* hC1, MCHANDLE* hC2, int Board1, int Board2, char* camfile1, char* camfile2);
EXPORT int stopAQDual(tThrDual *psPhxSnap);
//EXPORT int stopAQDual(tThrDual *psPhxSnap);
EXPORT int startAQDual(tThrDual *psPhxSnap, int numBuffers); 
EXPORT int PrepareDualChannel(MCHANDLE hChannel1, MCHANDLE hChannel2, tThrDual** ppsPhxSnap);
DWORD WINAPI GrabDualThreadProc(LPVOID Param);
int initChannelSurface(MCHANDLE hChannel,tSurfaceInfo* ptThr);
int setupAQ(MCHANDLE hChannel);
EXPORT int OpenDualChannel(MCHANDLE* hC1, MCHANDLE* hC2, int Board1, int Board2, char* camfile1, char* camfile2);
void loadUpBufferDual(tThrDual *psPhxSnap, BYTE *buffer, int bufferidx,int right);
EXPORT int grabFromBufferAsyncDual(tThrDual *psPhxSnap, void *pBlock, long long* plTimeStamps, int* fFrameCounters, int frames,  int grabtail);
EXPORT int framesReadyDual(tThrDual *psPhxSnap);
EXPORT void getstatsdual(tThrDual *psPhxSnap, double* fps, ui32* lastgrabbed );

