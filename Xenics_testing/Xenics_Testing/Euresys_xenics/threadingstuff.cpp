#include <stdio.h>
#include <stdlib.h>


#include "euresysnick.h" //<-- windows.h included from here...



#ifdef WIN32

	BOOL WINAPI consoleHandler( DWORD signal)
	{
		switch( signal) 
		{
		case CTRL_C_EVENT:
		case CTRL_CLOSE_EVENT:
			AquireApiLock();
			//VmbShutdown();
			ReleaseApiLock();
		}
		return TRUE;
	}

#endif


#ifdef WIN32
   	CRITICAL_SECTION critical;
	BOOL AquireApiLock()
	{
		EnterCriticalSection(&critical);	
		return TRUE;
	}

	BOOL ReleaseApiLock()
	{
		LeaveCriticalSection(&critical);	
		return TRUE;
	}

    
void jointhread(HANDLE hThread2)
{
    WaitForSingleObject(hThread2, INFINITE);
}





#elif LINUX
    pthread_mutex_t g_Mutex = PTHREAD_MUTEX_INITIALIZER;
    VmbBool_t CreateApiLock()
    {
        return VmbBoolTrue;
    }
    void DestroyApiLock()
    {
    }
    VmbBool_t AquireApiLock()
    {
        if(0 == pthread_mutex_lock( &g_Mutex ) )
        {
            return VmbBoolTrue;
        }
        return VmbBoolFalse;
    }
    void ReleaseApiLock()
    {
        pthread_mutex_unlock( &g_Mutex );
    }
#endif


#ifdef WIN32
	LARGE_INTEGER LIFrequency;


	long long getMicrotime(){
		//struct timespec currentTime;
		//gettimeofday(&currentTime, NULL);
	//clock_gettime(CLOCK_MONOTONIC ,&currentTime);
		//return currentTime.tv_sec * (long long)1e6 + currentTime.tv_nsec/1000;
		//long long StartingTime;
		LARGE_INTEGER ElapsedMicroseconds;
		
		QueryPerformanceCounter(&ElapsedMicroseconds);
		ElapsedMicroseconds.QuadPart *= 1000000;
		ElapsedMicroseconds.QuadPart /= LIFrequency.QuadPart;
		return ElapsedMicroseconds.QuadPart;
	}

#else
	/**
	 * Returns the current time in microseconds.
	 */
	long long getMicrotime(){
		struct timespec currentTime;
		//gettimeofday(&currentTime, NULL);
	clock_gettime(CLOCK_MONOTONIC ,&currentTime);
		return currentTime.tv_sec * (long long)1e6 + currentTime.tv_nsec/1000;
	}

#endif



int initDriver()
{
	MCSTATUS status;
	#ifdef WIN32
	QueryPerformanceFrequency(&LIFrequency);
	InitializeCriticalSectionAndSpinCount(&critical, 
        0x00000400) ;

	
	#endif

	status = McOpenDriver(NULL);
	if(status != MC_OK)
	{
		fprintf(stderr,"Error - Fail to initialize MultiCam\n");
		return -1;
	}

	status = McSetParamStr (MC_CONFIGURATION, MC_ErrorLog, "error.log");
	if(status != MC_OK)
	{
		fprintf(stderr,"Error - configuration log \n");
		return -1;
	}


}

void closeDriver()
{
	McCloseDriver();
}