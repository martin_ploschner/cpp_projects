#include "BobcatEthernet.h"

unsigned int enumerate_all_connected_xenics_cameras() {

	ErrCode errorCode = I_OK;
	unsigned int deviceCount = 0;

	if ((errorCode = XCD_EnumerateDevices(NULL, &deviceCount, XEF_EnableAll)) != I_OK) {
		printf("An error occurred while enumerating the devices. errorCode: %i\n", errorCode);
		return -1;
	}

	if (deviceCount == 0) {
		printf("Enumeration was a success but no devices were found!\n");
		return 0;
	}

	else {
		return deviceCount;
	}
}

int print_connected_cameras_info(unsigned int deviceCount) {

	ErrCode errorCode = I_OK;

	XDeviceInformation* devices = new XDeviceInformation[deviceCount];
	if ((errorCode = XCD_EnumerateDevices(devices, &deviceCount, XEF_UseCached)) != I_OK) {
		printf("Error while retrieving the cached device information structures. errorCode: %i\n", errorCode);
		delete[] devices;
		return -1;
	}

	for (unsigned int i = 0; i < deviceCount; i++) {
		XDeviceInformation* dev = &devices[i];
		printf("device[%lu] %s @ %s (%s) \n", i, dev->name, dev->address, dev->transport);
		printf("PID: %4X\n", dev->pid);
		printf("Serial: %lu\n", dev->serial);
		printf("URL: %s\n", dev->url);
		printf("State: %s\n\n", dev->state == XDS_Available ? "Available" : dev->state == XDS_Busy ? "Busy" : "Unreachable");
	}
}

XCHANDLE init_camera(char* URL) {
	XCHANDLE handle = 0; // Handle to the camera
	// Open a connection to the first detected camera by using connection string cam://0
	printf("Opening connection to %s\n",URL);
	handle = XC_OpenCamera(URL);
	if (XC_IsInitialised(handle))
	{
		printf("Camera was initialised successfully\n");
		return handle;
	}
}


int get_frame(XCHANDLE handle, unsigned short* frame) {
	unsigned long frame_size = 0;

	ErrCode errorCode = I_OK;

	if (XC_IsInitialised(handle)) {

		if ((errorCode = XC_StartCapture(handle)) != I_OK)
		{
			printf("Could not start capturing, errorCode: %lu\n", errorCode);
		}
		else if (XC_IsCapturing(handle))
		{
			frame_size = XC_GetFrameSize(handle);

			if ((errorCode = XC_GetFrame(handle, FT_NATIVE, XGF_Blocking, frame, frame_size)) != I_OK)
			{
				printf("Problem while fetching frame, errorCode %lu", errorCode);
			}
		}
	}

	return 0;
}