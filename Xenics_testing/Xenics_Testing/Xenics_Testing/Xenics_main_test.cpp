//#include "stdio.h" // C Standard Input/Output library.
//#include "XCamera.h" // Xeneth SDK main header.

#include "BobcatEthernet.h"

//#include "euresysnick.h" 

//int main()
//{
//	// ============================= Euresys Board initialisation ==============================================
//	MCHANDLE chan1;
//	MCHANDLE chan2;
//	int board1 = 1;
//	int board2 = 0;
//	char* camfile1 = "C:/LAB/Coding/CPP/Xenics_testing/Xenics_Testing/Euresys_xenics/CHEETAH_A.cam";
//	char* camfile2 = "C:/LAB/Coding/CPP/Xenics_testing/Xenics_Testing/Euresys_xenics/CHEETAH_B.cam";
//
//	OpenDualChannel(&chan1, &chan2, board1, board2, camfile1, camfile2);
//	// =========================================================================================================
//	return 0;
//}

int main()
{
	ErrCode errorCode = I_OK;
	
	unsigned int deviceCount = 0;
	deviceCount = enumerate_all_connected_xenics_cameras();

	/*  At this point we know how much devices are present in the environment.
	*  Now allocate the XDeviceInformation array to accommodate all the discovered devices using
	*  the device count to determine the size needed. Once allocated we call the enumerate
	*  devices method again but now instead of passing null as the initial argument use the new
	*  allocated buffer. For the flags argument we no longer use a protocol enable flag but make use
	*  of the XEF_UseCached flag. On discovery devices are cached internally and as such we are able to
	*  retrieve the device information structures instantly when calling XCD_EnumerateDevices for a second time.
	*  Note that it is not required to first check device count, allocate structure and retrieve cached devices.
	*  A user could allocate one or more device structure and immediately pass this with the initial call to XCD_EnumerateDevices.
	*  XCD_EnumerateDevices will not exceed the supplied deviceCount and when less devices were discovered than the initial deviceCount
	*  this argument is updated with the new count. */

	XDeviceInformation *devices = new XDeviceInformation[deviceCount];
	if ((errorCode = XCD_EnumerateDevices(devices, &deviceCount, XEF_UseCached)) != I_OK) {
		printf("Error while retrieving the cached device information structures. errorCode: %i\n", errorCode);
		delete[] devices;
		return -1;
	}

	/*  All discovered devices are now available in our local array and we are now able
	*  to iterate the list and output each item in the array */

	for (unsigned int i = 0; i < deviceCount; i++) {
		XDeviceInformation * dev = &devices[i];
		printf("device[%lu] %s @ %s (%s) \n", i, dev->name, dev->address, dev->transport);
		printf("PID: %4X\n", dev->pid);
		printf("Serial: %lu\n", dev->serial);
		printf("URL: %s\n", dev->url);
		printf("State: %s\n\n", dev->state == XDS_Available ? "Available" : dev->state == XDS_Busy ? "Busy" : "Unreachable");
	}
	

	// Variables
	XCHANDLE handle = 0; // Handle to the camera
	errorCode = 0; // Used to store returned errorCodes from the SDK functions.
	word *frameBuffer = 0; // 16-bit buffer to store the capture frame.
	dword frameSize = 0; // The size in bytes of the raw image.


	// Open a connection to the first detected camera by using connection string cam://0
	printf("Opening connection to cam://0\n");
	handle = XC_OpenCamera("cam://0");
	if (XC_IsInitialised(handle))
	{
		printf("Camera was initialised successfully\n");
	}

	// When the connection is initialised, ...
	if (XC_IsInitialised(handle))
	{
		long bitsize = XC_GetBitSize(handle);
		printf("Current bitsize of the camera is : %d-bit\n", bitsize == 0 ? 8 : 16);

		double exposure_time = 200;
		double exp_time_value;
		XC_GetPropertyValueF(handle, "ExposureTime", &exp_time_value);
		printf("The ExposureTime is currently set to : %f us\n", exp_time_value);

		XC_SetPropertyValueF(handle, "ExposureTime", exposure_time,""); // Ethernet Bobcat seems to support only "" as unit. The "us" worked for CL Bobcat but well it is Xenics after all

		XC_GetPropertyValueF(handle, "ExposureTime", &exp_time_value);
		printf("The ExposureTime was set to : %f us\n", exp_time_value);

		// ... start capturing
		printf("Start capturing.\n");
		if ((errorCode = XC_StartCapture(handle)) != I_OK)
		{
			printf("Could not start capturing, errorCode: %lu\n", errorCode);
		}
		else if (XC_IsCapturing(handle)) // When the camera is capturing ...
		{
			// Determine native framesize.
			frameSize = XC_GetFrameSize(handle);

			// Initialize the 16-bit buffer.
			frameBuffer = new word[frameSize / 2];

			// ... grab a frame from the camera.
			printf("Grabbing a frame.\n");
			if ((errorCode = XC_GetFrame(handle, FT_NATIVE, XGF_Blocking, frameBuffer, frameSize)) != I_OK)
			{
				printf("Problem while fetching frame, errorCode %lu", errorCode);
			}
		}
	}
	else
	{
		printf("Initialization failed\n");
	}

	// Cleanup.

	// When the camera is still capturing, ...
	if (XC_IsCapturing(handle))
	{
		// ... stop capturing.
		printf("Stop capturing.\n");
		if ((errorCode = XC_StopCapture(handle)) != I_OK)
		{
			printf("Could not stop capturing, errorCode: %lu\n", errorCode);
		}
	}

	// When the handle to the camera is still initialised ...
	if (XC_IsInitialised(handle))
	{
		printf("Closing connection to camera.\n");
		XC_CloseCamera(handle);
	}

	printf("Clearing buffers.\n");
	if (frameBuffer != 0)
	{
		delete[] frameBuffer;
		frameBuffer = 0;
	}

	delete[] devices;

	return 0;
}
