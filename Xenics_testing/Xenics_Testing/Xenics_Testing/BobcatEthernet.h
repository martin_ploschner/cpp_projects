#pragma once

#include "stdio.h" // C Standard Input/Output library.
#include "XCamera.h" // Xeneth SDK main header.


extern "C" __declspec(dllexport) unsigned int enumerate_all_connected_xenics_cameras();
extern "C" __declspec(dllexport) int print_connected_cameras_info(unsigned int deviceCount);
extern "C" __declspec(dllexport) XCHANDLE init_camera(char* URL);
extern "C" __declspec(dllexport) int get_frame(XCHANDLE handle, unsigned short* frame);