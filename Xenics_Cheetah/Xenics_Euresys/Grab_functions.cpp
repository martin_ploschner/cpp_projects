/*
+-------------------------------- DISCLAIMER ---------------------------------+
|                                                                             |
| This application program is provided to you free of charge as an example.   |
| Despite the considerable efforts of Euresys personnel to create a usable    |
| example, you should not assume that this program is error-free or suitable  |
| for any purpose whatsoever.                                                 |
|                                                                             |
| EURESYS does not give any representation, warranty or undertaking that this |
| program is free of any defect or error or suitable for any purpose. EURESYS |
| shall not be liable, in contract, in torts or otherwise, for any damages,   |
| loss, costs, expenses or other claims for compensation, including those     |
| asserted by third parties, arising out of or in connection with the use of  |
| this program.                                                               |
|                                                                             |
+-----------------------------------------------------------------------------+
*/

//INCASE YOU WANT TO COMPILE THIS INTO A DLL
//cl.exe /LD testGrab.cpp multicam.lib /I"C:/Program Files (x86)/Euresys/MultiCam/Include" -link -LIBPATH:"C:/Program Files (x86)/Euresys/MultiCam/Lib/"
//cl.exe /LD testGrab.cpp multicam.lib /I"C:/Program Files (x86)/Euresys/MultiCam/Include" -link -LIBPATH:"C:/Program Files (x86)/Euresys/MultiCam/Lib/amd64"
#include <stdio.h>
#include <stdlib.h>

#include "euresysnick.h"

BOOL error; // Flagged on acquisition error









int framesReady(tThr *psPhxSnap)
{
   ui32 framesReady = psPhxSnap->psPhxSnap->fBufferGrabbed - psPhxSnap->fLastGrabbed;

   if (framesReady>psPhxSnap->numBuffers)
   {
      framesReady = psPhxSnap->numBuffers;
   }

   return framesReady;

}

void getstats(tThr *psPhxSnap, double* fps, ui32* lastgrabbed )
{
   //return psPhxSnap->fps;
   *fps = psPhxSnap->fps;
   *lastgrabbed = psPhxSnap->psPhxSnap->fBufferGrabbed;
}


int grabFromBufferAsync(tThr *psPhxSnap, void *pBlock, long long* plTimeStamps, int* fFrameCounters, int frames,  int grabtail)
{
   //provide your own buffer! 
   //tail means that you fill just grab the last N frames regardless of how many are in the bufferZ

   tPhxSnap* pCont = psPhxSnap->psPhxSnap;
   ui32 dwPayloadSize = psPhxSnap->pitch*psPhxSnap->height;
   ui32 returnFlag = 0;

   if (pBlock ==0)
   {
      //Intend to allocate the array here....

   }
   //BLOCK THE API WHILE WE DO A COPY....
   AquireApiLock();
   // JUst grab the last
   if (  (framesReady(psPhxSnap) >= frames)  || grabtail>0 )
   {
      //printf("")
      if ( psPhxSnap->bufferidx >= frames   )
      {
       //  printf("NO LOOP\n");

         //Single memcpy
         memcpy(pBlock,   psPhxSnap->buffers +(psPhxSnap->bufferidx-frames)*dwPayloadSize,frames*dwPayloadSize   );

         memcpy(plTimeStamps,  psPhxSnap->timestamps      +   (psPhxSnap->bufferidx-frames),        frames*sizeof(long long)   );

         memcpy(fFrameCounters,psPhxSnap->ipFrameCounters +   (psPhxSnap->bufferidx-frames),        frames*sizeof(int)   );
         returnFlag = 1;
      }
      else
      {
         //printf("LOOP\n");
         ui32 tail  = frames - psPhxSnap->bufferidx; //how many to grab from the end and put in the front
         ui32 front = frames - tail; 

         memcpy(pBlock,                      psPhxSnap->buffers+(psPhxSnap->numBuffers-tail)*dwPayloadSize, tail*dwPayloadSize   );
         memcpy(((char*) pBlock) + tail*dwPayloadSize,   psPhxSnap->buffers,                                           front*dwPayloadSize   );

         memcpy(plTimeStamps, psPhxSnap->timestamps  +  (psPhxSnap->numBuffers-tail) , tail*sizeof(long long)   );
         memcpy(plTimeStamps+tail, psPhxSnap->timestamps,                                           front*sizeof(long long)   );

         //Copy TimeStamp Information
         memcpy(fFrameCounters,                      psPhxSnap->ipFrameCounters  + (psPhxSnap->numBuffers - tail)  ,  tail*sizeof(int)   );
         memcpy(fFrameCounters+tail ,     psPhxSnap->ipFrameCounters,                                            front*sizeof(int)   );


         returnFlag = 1;
         /* code */
      }
      
      if (grabtail==0)
      {
         //Only reset the frame counter if tail >
         psPhxSnap->fLastGrabbed = pCont->fBufferGrabbed;
      }
   }
   else
   {

   }
   ReleaseApiLock();

   return returnFlag;


}


void status(tThr *psPhxSnap)
{
   tPhxSnap* pCont = psPhxSnap->psPhxSnap;
   printf("status %d,%d,%d\n",pCont->fBufferCount,pCont->fBufferGrabbed,pCont->fFrameCount);
   printf("async buffidx %d\n",psPhxSnap->bufferidx);
}

MCSTATUS ExternalTrigger(MCHANDLE hChannel)
{
	printf("Setting Up External Triger");
	// //MC_CC1Usage_DIN1
	// MCHANDLE hChannel = psPhxSnap->hCamera;
	MCSTATUS status = MC_OK;

	status = McSetParamInt(hChannel, MC_CC1Usage, MC_CC1Usage_IIN1);
	return status;
	
}
//MC_CC1Usage_DIN1

MCSTATUS softTriggerCC1(MCHANDLE hChannel)
{
	//MC_OutputState
	MCSTATUS status = MC_OK;
	printf("SOFT TRIGGER");
	// status = McSetParamInt(hChannel, MC_CC1Usage, MC_CC1Usage_LOW);
	// printf("%d",status);
	// if (status != MC_OK)
	// 	printf("CANT SET CC1 LOW");
	// Sleep(1);
	
	// 	status = McSetParamInt(hChannel, MC_CC1Usage, MC_CC1Usage_HIGH);
	// if (status != MC_OK)
	// 	printf("CANT SET CC1 HIGH");
	// Sleep(1);
	
	// status = McSetParamInt(hChannel, MC_CC1Usage, MC_CC1Usage_LOW);
	// if (status != MC_OK)
	// 	printf("CANT SET CC1 LOW");

	

	int board = 0 ;
	int output = 7;
	//status = McSetParamInt(MC_BOARD+board,MC_OutputConfig+7,MC_OutputConfig_SOFT);
	//status = McSetParamInt(hC,MC_CC1Usage,MC_CC1Usage_SOFT);
	status = McSetParamInt(MC_BOARD+board,MC_OutputConfig+output,MC_OutputConfig_FREE);

	if (status != MC_OK)
			printf("Output %i not available for selected board, select another output\n", output);
	status = McSetParamInt(MC_BOARD+board,MC_OutputState + output,MC_OutputState_HIGH);
	
	if (status != MC_OK)
			printf("Could not set CC1 High\n");
	
	Sleep(1);
	status = McSetParamInt(MC_BOARD+board,MC_OutputState + output,MC_OutputState_LOW);
	if (status != MC_OK)
			printf("Could not set CC1 LOW\n");

	ExternalTrigger(hChannel);
	return status;
}





void McCallback(PMCCALLBACKINFO CallBackInfo)
{
	MCHANDLE hSurface;
	BYTE *pImage;
	tThr* ptThr;

	switch(CallBackInfo->Signal)
	{
	case MC_SIG_SURFACE_PROCESSING:
		hSurface = (MCHANDLE) CallBackInfo->SignalInfo;
		ptThr = (tThr*) CallBackInfo->Context;

		McGetParamPtr(hSurface,MC_SurfaceAddr,(PVOID*)&pImage);
		loadUpBuffer(ptThr,pImage);

		McGetParamInt64(hSurface, MC_TimeStamp_us, &(ptThr->psPhxSnap->lBufferTime));

		//lastframe = *cframe;
		//McGetParamInt(hSurface, MC_TimeCode, cframe);
		
		ptThr->psPhxSnap->fBufferCount++;
      	ptThr->psPhxSnap->fFrameCount++;
      	//ptThr->psPhxSnap->lBufferTime = getMicrotime();

		McSetParamInt(hSurface,MC_SurfaceState,MC_SurfaceState_FREE);
		break;
	case MC_SIG_ACQUISITION_FAILURE:
	// psPhxSnap->fFrameCount++;
		fprintf(stderr, "Acquisition Failure. Is a video source connected?\n");
		error = TRUE;
		break;
	default:
		fprintf(stderr,"Signal not handled: %d", CallBackInfo->Signal);
		error = TRUE;
		break;
	}
}



void loadUpBuffer(tThr *psPhxSnap,BYTE *buffer)
{
   //Don't need to implement this one until the cheeettAAAH

   tPhxSnap* pCont = psPhxSnap->psPhxSnap;

  
   ui32 dwPayloadSize = psPhxSnap->pitch*psPhxSnap->height;

   long long* timestamps =psPhxSnap->timestamps;// malloc(psPhxSnap->numBuffers*sizeof(long long));
   int* ipFrameCounters = psPhxSnap->ipFrameCounters;//malloc(psPhxSnap->numBuffers*sizeof(int));
   ui8* buffers = psPhxSnap->buffers;//# malloc( psPhxSnap->numBuffers*dwPayloadSize );

   psPhxSnap->buffers    = buffers;
   psPhxSnap->timestamps = timestamps;
   psPhxSnap->ipFrameCounters = ipFrameCounters;
   //should keep track of frame timing also....


	//PROTECT US FROM the grab async!!! 
	AquireApiLock();
	
	memcpy(buffers+psPhxSnap->bufferidx*dwPayloadSize,buffer,dwPayloadSize);
	timestamps[psPhxSnap->bufferidx] = pCont->lBufferTime;
	ipFrameCounters[psPhxSnap->bufferidx] = pCont->fFrameCount;

	if ( (psPhxSnap->bufferidx) > 20)
	{
	//calculate the fps
	psPhxSnap->fps =(double) (20000000/((double) (timestamps[psPhxSnap->bufferidx]-timestamps[(psPhxSnap->bufferidx)-20])));
	}
	
	psPhxSnap->bufferidx++;
	if (psPhxSnap->bufferidx >= psPhxSnap->numBuffers) psPhxSnap->bufferidx=0;

	//Should dump it into our allocated memory????  the numpy array...
	pCont->fBufferGrabbed ++;
	ReleaseApiLock();

   }

int OpenSerial(int board,char* port)
{
	//NEEDS SOME ADMIN RIGHTS...  Unfortunately cannot figure out how to get this to work....

	MCSTATUS status = MC_OK;
	status = McSetParamStr(MC_BOARD+board, MC_SerialControlA, port);

	return status;
	
}



int OpenChannelSingle(MCHANDLE* hC,int Board, char* camfile)
{
	MCSTATUS status = MC_OK;
	MCHANDLE& hChannel = *hC;
	//  Create a channel.
	status = McCreate(MC_CHANNEL, &hChannel);
	if (status != MC_OK) goto Finalize;
	
	// Link the channel to a board. Here we take the first board.
	status = McSetParamInt(hChannel, MC_DriverIndex, Board);
	if (status != MC_OK) goto Finalize;
	status = McSetParamStr(hChannel, MC_Connector, "M");

	if (camfile[0] != 0)
	{
	printf("%s\n",camfile);
	status = McSetParamStr(hChannel, MC_CamFile, camfile);
	if (status != MC_OK) goto Finalize;
	}
	return status;
	Finalize:
	if (status != MC_OK)
	{
		printf("InitChannel Error %d\n",status);
	}

	if (hChannel)
	{
		McDelete(hChannel);
	}
	return status;
}


//
int setupROI(MCHANDLE hChannel, int width, int height)
{
	// MC_Hactive_Px
	// Hactive_Px          = 320;
    // Vactive_Ln          = 257;
	MCSTATUS status = MC_OK;

	status = McSetParamInt(hChannel, MC_Hactive_Px, width);
	status = McSetParamInt(hChannel, MC_Vactive_Ln, height);

	printf("Setting up ROI Error Code %d\n", status);

	return 0;
}

//  Create a channel and set its parameters
int PrepareSingleChannel(MCHANDLE hChannel, tThr** ppsPhxSnap) //(tHandle* hCamera, tThr** ppsPhxSnap, ui32 async, ui32 numbuffers  )
{

	MCSTATUS status = MC_OK;
	


   tPhxSnap* psPhxSnap;
   tThr* ptThr;
   
   psPhxSnap = (tPhxSnap *) calloc(1,sizeof(tPhxSnap)    );
   ptThr     = (tThr *) calloc(1,sizeof(tThr)    );


	int i;

	int sizeX = 0;
	int sizeY = 0;
	int pitch = 0;

	// Set the acquisition mode
	McSetParamInt(hChannel, MC_AcquisitionMode, MC_AcquisitionMode_SNAPSHOT);
	if (status != MC_OK) goto Finalize;

	// Choose the number of lines per page
	McSetParamInt(hChannel, MC_SeqLength_Fr, MC_INDETERMINATE);
	if (status != MC_OK) goto Finalize;

	// Choose the way the first acquisition is triggered
	McSetParamInt(hChannel, MC_TrigMode, MC_TrigMode_IMMEDIATE);
	if (status != MC_OK) goto Finalize;

	// Choose the triggering mode for subsequent acquisitions
	McSetParamInt(hChannel, MC_NextTrigMode, MC_NextTrigMode_SAME);
	if (status != MC_OK) goto Finalize;

	status = McSetParamInt(hChannel,MC_AcqTimeout_ms,-1);
	

	// Retrieve channel size information.
	status= McGetParamInt(hChannel, MC_ImageSizeX, &ptThr->width);
	if (status != MC_OK) goto Finalize;
	status= McGetParamInt(hChannel, MC_ImageSizeY, &ptThr->height);
	if (status != MC_OK) goto Finalize;  
	status= McGetParamInt(hChannel, MC_BufferPitch, &ptThr->pitch);
	if (status != MC_OK) goto Finalize;  

	printf("%d,%d,%d",ptThr->width,ptThr->height,ptThr->pitch);

	// The number of images to acquire.
	//status = McSetParamInt (hChannel, MC_SeqLength_Fr, MC_INDETERMINATE);
	//if (status != MC_OK) goto Finalize;

	//McSetParamInt(hChannel,MC_MaxFillingSurfaces,10); //10 buffers
	McSetParamInt(hChannel, MC_SurfaceCount, 40);
	McSetParamInt(hChannel,MC_MaxFillingSurfaces,MC_MaxFillingSurfaces_MAXIMUM);

	// Register our Callback function for the MultiCam asynchronous signals.
	status = McRegisterCallback(hChannel,  (PMCCALLBACK) McCallback, ptThr);
	if (status != MC_OK) goto Finalize;

	// Enable the signals we need: 
	// MC_SIG_SURFACE_PROCESSING: acquisition done and locked for processing
	// MC_SIG_ACQUISITION_FAILURE: acquisition failed.
	status = McSetParamInt(hChannel, MC_SignalEnable + MC_SIG_SURFACE_PROCESSING, MC_SignalEnable_ON);
	if (status != MC_OK) goto Finalize;

	status = McSetParamInt(hChannel, MC_SignalEnable + MC_SIG_ACQUISITION_FAILURE, MC_SignalEnable_ON);
	if (status != MC_OK) goto Finalize;


	psPhxSnap->running = FALSE;
    ptThr->psPhxSnap   = psPhxSnap;
    ptThr->hCamera     = hChannel;
    //ptThr->numBuffers  = numbuffers; //TEST...
    ptThr->dwGrabbed    = 0;
    ptThr->dwAcquired   = 0;
    //ptThr->thread_id     = 0;
    ptThr->fLastGrabbed  = 0;

	*ppsPhxSnap  = ptThr; ///Set the pointer handle

	return 0;

Finalize:
	if (status != MC_OK)
	{
		printf("InitChannel Error %d\n",status);
	}


	return status;

}

// Start the image acquisition, images are processed in the callback
int startAQ(tThr *psPhxSnap, int numBuffers) 
{
	MCHANDLE hChannel = psPhxSnap->hCamera;
	MCSTATUS status = 0;

	//MAYBE WE MAKE ALL THE BUFFERS HERE?
	
   tPhxSnap* pCont = psPhxSnap->psPhxSnap;

   ui32 dwlastbuffer = 0;
   ui32 dwBuffersAvailable = 0;
   
   ui8* buffer;
   
   ui32 dwPayloadSize = psPhxSnap->pitch*psPhxSnap->height;

   psPhxSnap->numBuffers = numBuffers;

   int buffersReady;
   long long* timestamps = (long long*) malloc(psPhxSnap->numBuffers*sizeof(long long));
   int* ipFrameCounters = (int*) malloc(psPhxSnap->numBuffers*sizeof(int));
   
   //make a bunch of buffers
   psPhxSnap->bufferidx = 0;
   ui8* buffers =  (ui8*) malloc( psPhxSnap->numBuffers*dwPayloadSize );

   psPhxSnap->buffers    = buffers;
   psPhxSnap->timestamps = timestamps;
   psPhxSnap->ipFrameCounters = ipFrameCounters;


	// Start Acquisitions for this channel.
	pCont->running = TRUE; 
	status = McSetParamInt(hChannel, MC_ChannelState, MC_ChannelState_ACTIVE);
	if (status != MC_OK) goto Finalize;


	return 0;

	

Finalize:
	if (status!=MC_OK)
	{
		pCont->running = FALSE;
		printf("Acquire Error %d\n",status);
	}

	if (hChannel)
	{
		McDelete(hChannel);
	}
}

int stopAQ(tThr *psPhxSnap)
{
	MCHANDLE hChannel = psPhxSnap->hCamera;
	MCSTATUS status = 0;

	psPhxSnap->psPhxSnap->running = FALSE;
	status= McSetParamInt(hChannel, MC_ChannelState, MC_ChannelState_IDLE);

	//FREEEDOM!!!
   free(psPhxSnap->buffers);
   free(psPhxSnap->timestamps);
   free(psPhxSnap->ipFrameCounters);
   return 0;

}



//McCloseDriver();
// Application entry point
int main(int argc, char* argv[])
{
	MCSTATUS status;
	MCHANDLE hChannel = 0;
	error = FALSE;


	return 0;
}

