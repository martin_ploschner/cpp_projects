#include "stdio.h" // C Standard Input/Output library.
#include "XCamera.h" // Xeneth SDK main header.

#include "euresysnick.h"

using namespace System;
using namespace System::IO;
using namespace System::Threading;


ref class wrapperForGrabber
{
	public:
		tThrDual* EuresysThreadPointer;
		void SetPointer(tThrDual*);
		void GrabbingThreadProc();
};

void wrapperForGrabber::SetPointer(tThrDual* psPhxSnap) {
	EuresysThreadPointer = psPhxSnap;
}

void wrapperForGrabber::GrabbingThreadProc()
{
	GrabDualCLRThreadProc(wrapperForGrabber::EuresysThreadPointer);
}

int main()
{
	// ============================= Find Cheetah and set it up =====================================================
	ErrCode errorCode = I_OK;
	unsigned int deviceCount = 0;

	if ((errorCode = XCD_EnumerateDevices(NULL, &deviceCount, XEF_EnableAll)) != I_OK) {
		printf("An error occurred while enumerating the devices. errorCode: %i\n", errorCode);
		return -1;
	}
	
	if (deviceCount == 0) {
		printf("Enumeration was a success but no devices were found!\n");
		return 0;
	}

	XDeviceInformation *devices = new XDeviceInformation[deviceCount];
	if ((errorCode = XCD_EnumerateDevices(devices, &deviceCount, XEF_UseCached)) != I_OK) {
		printf("Error while retrieving the cached device information structures. errorCode: %i\n", errorCode);
		delete[] devices;
		return -1;
	}
	
	for (unsigned int i = 0; i < deviceCount; i++) {
		XDeviceInformation * dev = &devices[i];
		printf("device[%lu] %s @ %s (%s) \n", i, dev->name, dev->address, dev->transport);
		printf("PID: %4X\n", dev->pid);
		printf("Serial: %lu\n", dev->serial);
		printf("URL: %s\n", dev->url);
		printf("State: %s\n\n", dev->state == XDS_Available ? "Available" : dev->state == XDS_Busy ? "Busy" : "Unreachable");
	}

	// Variables
	XCHANDLE handle = 0; // Handle to the camera
	errorCode = 0; // Used to store returned errorCodes from the SDK functions.
	word *frameBuffer = 0; // 16-bit buffer to store the capture frame.
	dword frameSize = 0; // The size in bytes of the raw image.
	
	
	// Open a connection to the first detected camera by using connection string cam://0
	printf("Opening connection to cam://0\n");
	handle = XC_OpenCamera("cam://0");
	if (XC_IsInitialised(handle))
	{
		printf("Camera was initialised successfully\n");
	}

	

	
	if (XC_IsInitialised(handle))
	{
		// This switches Cheetah into 12bit mode
		long pCamLinkQuad = 0;
		XC_SetPropertyValueL(handle, "CamLinkQuad", 1, ""); // 0 .. 1-byte per pixel; 1 .. 2-bytes per pixel
		XC_GetPropertyValueL(handle, "CamLinkQuad", &pCamLinkQuad);
		printf("CamLinkQuad was set to: %s \n", (pCamLinkQuad == 0) ? "1-byte per pixel" : "2-bytes per pixel");


		// Check the pixel bitsize just in case
		long bitsize = XC_GetBitSize(handle);
		printf("Current bitsize of the camera is : %d-bit\n", bitsize == 0 ? 8 : 16);
	    
		// Set the Integration time
		double exposure_time_set = 50;
		double exposure_time_get;
		XC_GetPropertyValueF(handle, "IntegrationTime", &exposure_time_get);
		printf("The IntegrationTime is currently set to : %f us\n", exposure_time_get);
	
		XC_SetPropertyValueF(handle, "IntegrationTime", exposure_time_set, "us");
	
		XC_GetPropertyValueF(handle, "IntegrationTime", &exposure_time_get);
		printf("The IntegrationTime was set to : %f us\n", exposure_time_get);

		//long captureMode = 2; // 2 = rising edge
		//XC_SetPropertyValueL(handle, "TriggerMode", captureMode, "");
		//XC_GetPropertyValueL(handle, "TriggerMode", &captureMode);
		//printf("The captureMode was set to : %d\n", captureMode);

		//long triggerSource = 2; // 2 = CameraLinkCC1
		//XC_SetPropertyValueL(handle, "TriggerSource", triggerSource, "");
		//XC_GetPropertyValueL(handle, "TriggerSource", &triggerSource);
		//printf("The triggerSource was set to : %d\n", triggerSource);
	
	}
	else
	{
		printf("Initialization failed\n");
	}

	
	// ============================= Euresys Board initialisation ==============================================
	int frames = 1;
	unsigned short* buffer = (unsigned short*)malloc(sizeof(unsigned short)* frames * 640 * 512);
	int* framecounters = (int*)malloc(sizeof(int)* frames * 2);
	long long* timestamps = (long long*)malloc(sizeof(long long)* frames * 2);
	double* fps = (double*) malloc(sizeof(double));
	ui32* lastgrabbed = (ui32*)malloc(sizeof(ui32));
	int test_framesready = 0;



	initDriver();

	MCHANDLE chan1;
	MCHANDLE chan2;
	int board1 = 1;
	int board2 = 0;
	char* camfile1 = "C:/LAB/Coding/CPP/Xenics_testing/Xenics_Testing/Euresys_xenics/CHEETAH_A.cam";
	char* camfile2 = "C:/LAB/Coding/CPP/Xenics_testing/Xenics_Testing/Euresys_xenics/CHEETAH_B.cam";
	OpenDualChannel(&chan1, &chan2, board1, board2, camfile1, camfile2);
	printf("Euresys channels opened\n");

	setupROI(chan1, 320, 512); //needs to be divided in half
	setupROI(chan2, 320, 512);


	tThrDual* tThrDualEuresys = PrepareDualChannel(chan1, chan2);

	tThrDualEuresys = startAQDual_without_launching_thread(tThrDualEuresys, 4000); //initial setup

	wrapperForGrabber^ newGrabClass = gcnew wrapperForGrabber;
	newGrabClass->SetPointer(tThrDualEuresys);

	Thread^ EuresysDualGrabThread;
	EuresysDualGrabThread = gcnew Thread(gcnew ThreadStart(newGrabClass, &wrapperForGrabber::GrabbingThreadProc)); // ref class and the function we want to launch
	EuresysDualGrabThread->Priority = ThreadPriority::Highest;
	EuresysDualGrabThread->Start();

	Console::WriteLine("Press ESC to stop");

	int frames_taken = 0;
	int iter_taken = 0;
	bool firstRun = true;
	ui32* missed_grabs = (ui32*)malloc(sizeof(ui32));
	while (!(Console::KeyAvailable && Console::ReadKey(true).Key == ConsoleKey::Escape ))
	{
		if (firstRun)
		{
			getstatsdual(tThrDualEuresys, fps, missed_grabs);
			firstRun = false;
		}
		if (framesReadyDual(tThrDualEuresys) >= frames) // By the time we get here the GrabbingThreadProc() function already takes some frames
		{			
			grabFromBufferAsyncDual_Martin(tThrDualEuresys, buffer, timestamps, framecounters, frames, 0);
			frames_taken++;
		}
		/*else if (framesReadyDual(tThrDualEuresys) > frames)
		{
			printf("Dude there are more frames in Nick's buffer than what you can handle!");
		}*/
		iter_taken++;
	}
	EuresysDualGrabThread->Abort();

	getstatsdual(tThrDualEuresys, fps, lastgrabbed);
	printf("Nick's FPS was %lf \n", *fps);
	printf("Nick's lastgrabbed IDX = %d\n", *lastgrabbed);
	printf("Nick's framecounter IDX(left,right) = (%d, %d)\n", *(&framecounters[0]), *(&framecounters[1]));
	printf("C++/CLI framecounter IDX = %d\n", frames_taken);
	printf("Missed frames= %d\n", *missed_grabs);
	printf("C++ grab frame loop went through %d iterations\n", iter_taken);
	
	
	stopAQDual(tThrDualEuresys);
	closeDriver();



	
	/*ParameterizedThreadStart^ threadDelegate;
	threadDelegate = gcnew ParameterizedThreadStart(this, &cameraForm::cameraInitRoutine);
	Thread^ initThread = gcnew Thread(threadDelegate);
	initThread->Start(cameras[camIdx]);*/
	

	//startAQDual(tThrDualEuresys, 4000); //Creates circular Euresys buffer
	//Sleep(3000);
	//

	//Console::WriteLine("Press ESC to stop");

	//int frames_taken = 0;
	//int iter_taken = 0;
	//ResumeThread(tThrDualEuresys->pThread);
	//Sleep(100);
	//SuspendThread(tThrDualEuresys->pThread);
	//int current_thread_priority = GetThreadPriority(tThrDualEuresys->pThread);
	//printf("Thread priority is currently set to %d\n", current_thread_priority);
	//Sleep(100);
	//ResumeThread(tThrDualEuresys->pThread);
	//while (!(Console::KeyAvailable && Console::ReadKey(true).Key == ConsoleKey::Escape))
	//{
	//	if (framesReadyDual(tThrDualEuresys) >= frames)
	//	{
	//		grabFromBufferAsyncDual(tThrDualEuresys, buffer, timestamps, framecounters, frames, 0);
	//		frames_taken++;
	//	}
	//	iter_taken++;
	//}

	//getstatsdual(tThrDualEuresys, fps, lastgrabbed);
	//printf("FPS is %lf \n", *fps);
	//printf("Last frame grabbed %d \n", *lastgrabbed);
	//printf("Frames grabbed %d \n", frames_taken);
	//printf("Iterations taken %d \n", iter_taken);
	//
	//
	//


	//stopAQDual(tThrDualEuresys);
	//closeDriver();
	//// =========================================================================================================


	//// ========================== Stop Cheetah ================================================================
	//	
	//// When the handle to the camera is still initialised ...
	//if (XC_IsInitialised(handle))
	//{
	//	printf("Closing connection to camera.\n");
	//	XC_CloseCamera(handle);
	//}
	//
	//delete[] devices;
	//// =========================================================================================================

	//// ========================== Saving files and free memory =================================================================
	//FILE* pfBuffer;
	//pfBuffer = fopen("C:/LAB/Coding/Python/Data/Cheetah_frames/buffer.dat", "wb");
	//fwrite(buffer, sizeof(unsigned short), frames*640*512, pfBuffer);
	//fclose(pfBuffer);

	//FILE* pfFramecounters;
	//pfFramecounters = fopen("C:/LAB/Coding/Python/Data/Cheetah_frames/framecounters.dat", "wb");
	//fwrite(framecounters, sizeof(int), frames * 2, pfFramecounters);
	//fclose(pfFramecounters);

	//FILE* pfTimestamps;
	//pfTimestamps = fopen("C:/LAB/Coding/Python/Data/Cheetah_frames/timestamps.dat", "wb");
	//fwrite(timestamps, sizeof(long long), frames * 2, pfTimestamps);
	//fclose(pfTimestamps);


	//Console::WriteLine("Files have been saved.");

	return 0;
}



// ================================== Test code =====================================================

//ref class EuresysDual
//{
//public:
//
//	void Euresys_StartAQDual()
//	{
//
//	}
//
//};


/*EuresysDual^ mydual = gcnew EuresysDual;
ThreadStart^ threadDelegate;
threadDelegate = gcnew ThreadStart(mydual, &EuresysDual::Euresys_StartAQDual);
Thread^ EuresysGrabberThread = gcnew Thread(threadDelegate);
EuresysGrabberThread->Name = "Euresys thread to fill circular buffer";
EuresysGrabberThread->Start();*/


/*int frames_taken = 0;
int iter_taken = 0;
while (frames_taken <= 30000)
{
if (framesReadyDual(tThrDualEuresys) >= frames)
{
grabFromBufferAsyncDual(tThrDualEuresys, buffer, timestamps, framecounters, frames, 0);
frames_taken++;
}
iter_taken++;
}
getstatsdual(tThrDualEuresys, fps, lastgrabbed);
printf("FPS is %lf \n", *fps);
printf("Last frame grabbed %d \n", *lastgrabbed);
printf("Frames grabbed %d \n", frames_taken);
printf("Iterations taken %d \n", iter_taken);*/